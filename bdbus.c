/*
 * bdbus.c - some DBus routines
 *
 * by Robert (bob) Edwards, August 2023
 *
 * GPLv2 etc.
 */

#include <errno.h>
#include <pthread.h>

#include "bdbus.h"

/* https://0pointer.net/blog/the-new-sd-bus-api-of-systemd.html */
static void *bdbus_dispatcher (void *arg) {
	struct bdbus_disp_node *node = (struct bdbus_disp_node *)arg;
	int rv;
	
	for (;;) {
		/* Process requests */
		rv = sd_bus_process (node->bus, NULL);
		if (rv < 0) {
			errno = -rv;
			BERR ("sd_bus_process");
			continue;
		}
		if (rv > 0) continue;

		/* Wait for the next request to process */
		rv = sd_bus_wait (node->bus, (uint64_t) -1);
		if (rv < 0) {
			errno = -rv;
			BERR ("sd_bus_wait");
		}
	}
	return NULL;
} /* bdbus_dispatcher () */

int bdbus_init (char *bus_path, const sd_bus_vtable *vtable) {
	sd_bus_slot *slot = NULL;
	sd_bus *bus = NULL;
	struct bdbus_disp_node *node = NULL;
	int rv;

	char *int_name = strdup (bus_path + 1);
	for (char *cp = int_name; *cp; cp++) {
		if ('/' == *cp) *cp = '.';
	}
	
	if ((rv = sd_bus_open (&bus)) < 0) {
		errno = -rv;
		BEMERG ("sd_bus_open");
	}

	rv = sd_bus_add_object_vtable (bus, &slot, bus_path, int_name, vtable, NULL);
	if (rv < 0) {
		errno = -rv;
		BEMERG ("sd_bus_add_object_vtable");
	}

	rv = sd_bus_request_name (bus, int_name, 0);
	if (rv < 0) {
		errno = -rv;
		BEMERG ("sd_bus_request_name ('%s')", int_name);
	}

	node = malloc (sizeof (struct bdbus_disp_node));
	if (!node) {
		BEMERG ("malloc");
	}
	node->bus = bus;
	if (pthread_create (&node->tid, NULL, bdbus_dispatcher, node)) {
		BEMERG ("pthread_create");
	}

	return 0;
} /* bdbus_init () */
