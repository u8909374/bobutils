/*
 * util.c - miscellaneous routines
 *
 * Copyright 2006, 2007, 2008, 2009, 2021 Robert Edwards, Australian National University
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <poll.h>

#include "util.h"

uint8_t dbglvl = LOG_ERR;
uint8_t _loglvl = 0;	// no logging by default
char *bu_dbgNames[] = {
	"EMERG",
	"ALERT",
	" CRIT",
	"ERROR",
	" WARN",
	" NOTE",
	" INFO",
	"DEBUG",
	"DBGP1",
	"DNGP2",
	NULL
};
char **_dbgNames = bu_dbgNames;
uint8_t bu_issd = 0;

char *month_names [] = {
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December",
	NULL
};

char *day_names [] = {
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	NULL
};

#ifdef TESTING
char *teststrs[] = {
	"Hello World!\n",
	"Hello",
    "World",
	" ",
	NULL
};
#endif /* TESTING */

int daemonize (void) {
	pid_t pid;

	if ((pid = fork ()) < 0) return -1;
	if (pid != 0) exit (0);	/* get rid of parent */
	/* now in child */
	if ((pid = fork ()) < 0) return -1;
	if (pid != 0) exit (0);	/* get rid of child */
	/* now in grandchild */
	setsid ();		/* become session leader */
	chdir ("/");
	umask (0);
	dbglvl = 0;		/* stop any writes to stderr */
	close (0);
	close (1);
	close (2);
	return 0;
} /* daemonize */

pid_t fork_exec (const char *cmd, char *const *args, char *const envp[]) {
	/* shamelessly lifted from SEI Cert C Coding Standard */
	pid_t pid;
	if ((pid = fork ()) < 0) {
		BERR ("fork");
		return -1;
	}
	if (pid == 0) {
		if (execve (cmd, args, envp) < 0) {
			BERR ("execve: %s", cmd);
			exit (0);
		}
	}
	return pid;
} /* fork_exec () */

void sigchld_handler (int s) {
	(void)s; // quiet unused variable warning

	// waitpid() might overwrite errno, so we save and restore it:
	int saved_errno = errno;

	while (waitpid (-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
} /* sigchld_handler () */

int setup_sigchld () {
	struct sigaction sa;
	sa.sa_flags = 0;
	sigemptyset (&sa.sa_mask);
	sa.sa_handler = sigchld_handler;
	return sigaction (SIGCHLD, &sa, NULL);
} /* setup_sigchld () */

/*
 * We can read from stdin if :
 * - we are in foreground
 * - stdin is a pipe end
 * from: https://stackoverflow.com/questions/3940555
 */
int is_foreground (void) {
    pid_t fg = tcgetpgrp (STDIN_FILENO);
    if (-1 == fg) {
		BDEBUG ("Piped");
    } else if (fg == getpgrp ()) {
		BDEBUG ("foreground");
    } else {
		BDEBUG ("background");
        return 0;
    }
    return 1;
} /* is_foreground () */

void strAppend (char *buf, char *join, char *new) {
//	if (buf && join && new) {
	if (buf) {
		if (join && strlen (buf) > 0) strcat (buf, join);
		if (new) strcat (buf, new);
	}
} /* strAppend () */

char *myStrCat (char *dest, int sz, const char *src) {
	char *p;
	if (!dest) return NULL;
	for (p = dest; *p; p++);
	sz -= (p - dest);
	if (sz <= 0) return NULL;
	if (src) while (--sz && (*p++ = *src++));
	*p = 0;
	return dest;
} /* myStrCat () */

/* useful strcmp for tsearch (), tfind (), tdelete () */
int const_strcmp (const void *l, const void *r) {
	return strcmp ((const char *)l, (const char *)r);
} /* const_strcmp () */

char *str2lower (char *str) {
	if (!str) return NULL;
	for (char *s = str; *s; s++) *s = tolower (*s);
	return str;
} /* str2lower () */

int hex2num (const char in) {
	if ((in >= '0') && (in <= '9')) return (in - '0');
	if ((in >= 'A') && (in <= 'F')) return (in - 'A' + 10);
	if ((in >= 'a') && (in <= 'f')) return (in - 'a' + 10);
	return -1;
} /* hex2num () */

int hex2num2 (char *hex) {
	int rv = hex2num (*hex++);
	if ((rv < 0) || (hex2num (*hex) < 0)) return -1;
	return rv * 16 + hex2num (*hex);
} /* hex2num2 () */

char num2hex (const int in) {
	if (in < 0) return '\0';
	if (in <= 9) return in + '0';
	if (in <= 15) return in + 'a' - 10;
	return '\0';
} /* num2hex () */

int strhex2bin (unsigned char *out, const char *in, int len) {
	int num_out = 0;
	int msn = 0;
	int lsn;
	int flag;

	if (!(in && out)) return -1;
	flag = len & 1;
	while (*in && len--) {
		if ((lsn = hex2num (*in++)) == -1) return -1;
		if (flag) {
			*out++ = msn * 16 + lsn;
			num_out++;
		} else {
			msn = lsn;
		}
		flag = !flag;
	}
	return (num_out);
} /* strhex2bin () */

void hex_encode (char *dst, const unsigned char *src, size_t srcSize) {
	while (srcSize--) {
		*dst++ = num2hex ((*src >> 4) & 0xf);
		*dst++ = num2hex (*src++ & 0xf);
	}
	*dst = '\0';
} /* hex_encode () */

char *str2Cs (char *buf, int len, const char *in) {
	char *p = buf;
	if (!(buf && len-- && in)) return NULL;
	while (len && *in) {
		int x = 0;
		if (isprint (*in)) {
			*p++ = *in++;
			len--;
			continue;
		}
		switch (*in) {
			case '\a': x = 'a'; break;
			case '\b': x = 'b'; break;
			case '\f': x = 'f'; break;
			case '\n': x = 'n'; break;
			case '\r': x = 'r'; break;
			case '\t': x = 't'; break;
			case '\v': x = 'v'; break;
			case '\\':
			case '\'':
			case '\"': x = *in; break;
		}
		if (x) {
			if (len > 1) {
				*p++ = '\\';
				*p++ = x;
				len -= 2;
			} else {
				break;
			}
		} else {
			char b[8];
			int n = snprintf (b, 7, "\\%03o", (unsigned char) *in);
			if (len >= n) {
				memcpy (p, b, n);
				p += n;
				len -= n;
			} else {
				break;
			}
		}
		in++;
	}
	*p = 0;
	return buf;
} /* str2Cs () */

/* count how many pointers in a NULL-terminated array */
inline int arrCount (void *arr) {
	int rv = 0;
	if (arr) {
		void **ar = arr;
		while (*ar++) rv++;
	}
	return rv;
} /* arrCount () */

/* find index of str needle in NULL-terminated array of strings haystack */
int strArrInd (const char *needle, char **haystack) {
	int rv = 0;
	if (!needle || !haystack) return -1;
	while (*haystack && strcmp (needle, *haystack)) {
		BDEBP1 ("'%s' is not '%s'", needle, *haystack);
		haystack++;
		rv++;
	}
	if (*haystack) return rv;
	return -1;
} /* strArrInd () */

void strArrFree (char **arr) {
	if (!arr) return;
	for (char **p = arr; *p; p++) free (*p);
	free (arr);
} /* strArrFree () */

int serialSetup (char *dev, int baud, int raw, int hshk) {
	speed_t speed;
	int fd;
	struct termios current;
	struct termios options;

	if (!dev && !*dev) {
		errno = EINVAL;
		return -1;
	}

	switch (baud) {
		case 600: speed = B600; break;
		case 1200: speed = B1200; break;
		case 2400: speed = B2400; break;
		case 4800: speed = B4800; break;
		case 9600: speed = B9600; break;
		case 19200: speed = B19200; break;
		case 38400: speed = B38400; break;
		case 57600: speed = B57600; break;
		case 115200: speed = B115200; break;
		case 230400: speed = B230400; break;
		default: return -1;
	}

	do {
		fd = open (dev, O_RDWR | O_NOCTTY);
	} while (fd == -1 && errno == EINTR);

	if (fd < 0) {
		return fd;
	}

	if (ioctl (fd, TIOCEXCL)) {
		close (fd);
		return -1;
	}

	if (tcgetattr (fd, &current) < 0) {
		close (fd);
		return -1;
	}

	memcpy (&options, &current, sizeof (options));
	cfsetispeed (&options, speed);
	cfsetospeed (&options, speed);

	if (raw) {
		cfmakeraw (&options);
	}

	// disable all flow control
	options.c_iflag &= ~(IXON | IXOFF | IXANY);
	options.c_oflag &= ~CRTSCTS;
	switch (hshk) {
		case SER_HANDSHAKE_HW:
			options.c_oflag |= CRTSCTS; break;
		case SER_HANDSHAKE_XON:
			options.c_iflag |= (IXON | IXOFF); break;
		default: break;
	}

	/* only attempt to set the attributes if they have changed */
	if (memcmp (&current, &options, sizeof (options))) {
		tcsetattr (fd, TCSANOW, &options);
		syslog (LOG_INFO, "changing serial port settings");
	}
	tcflush (fd, TCIOFLUSH);
	return fd;
} /* serialSetup () */

int set_nonblock (int fd) {
	return fcntl (fd, F_SETFL, (fcntl (fd, F_GETFL) | O_NONBLOCK));
} /* set_nonblock () */

/*
 * like read (2), but with a timeout...
 */
int read_timeout (int sock, void *buf, size_t count, int timeout) {
	struct pollfd pfd;

	pfd.fd = sock;
	pfd.events = POLLIN;
	while (poll (&pfd, 1, timeout) > 0) {
		if (pfd.revents & POLLIN) {
			return (read (sock, buf, count));
		}
	}
	return -timeout;
} /* read_timeout () */

char *getsNoEcho (int fd, char *str, int size) {
	struct termios current;
	struct termios options;
	FILE *file;
	char *retval = NULL;

	if (tcgetattr (fd, &current) == -1)
		return NULL;
	options = current;
	options.c_lflag &= ~ECHO;
	if (tcsetattr (fd, TCSANOW, &options) == -1)
		return NULL;
	if ((file = fdopen (fd, "r")))
		retval = fgets (str, size, file);
	tcsetattr (fd, TCSANOW, &current);
	return retval;
} /* getsNoEcho () */

/*
 * return a new buffer with a full path filename. If name starts with /
 * then new buffer has name, otherwise prepend dir and a "/"
 */
char *prependDir (char *name, char *dir) {
	char *ret;

	if (name[0] == '/') return (strdup (name));
	ret = malloc (strlen (name) + strlen (dir) + 2);
	if (ret) sprintf (ret, "%s/%s", dir, name);
	return ret;
} /* prependDir () */

int setPathMode (char *path, mode_t mode) {
	struct stat statbuf;
	mode_t new_mode;

	if (stat (path, &statbuf) < 0) {
		BDEBUG ("stat err");
		return -1;
	}
	new_mode = (statbuf.st_mode & ~0377) | mode;
	BDEBUG ("path: '%s': mode: %o", path, new_mode);
	return (chmod (path, new_mode));
} /*setPathMode () */

int ndirents (char *dirname) {
	int rv = 0;
	DIR *dir = opendir (dirname);
	if (!dir) return -1;
	while (readdir (dir)) rv++;
	closedir (dir);
	return rv;
} /* num_dir_ent () */

int num_threads () {
	return ndirents ("/proc/self/task") - 2;
} /* num_threads () */

long clock_ms () {
	struct timespec now;
	clock_gettime (CLOCK_BOOTTIME, &now);
	return ((now.tv_sec * 1000) + (now.tv_nsec / 1000000));
} /* clock_ms () */

long interval_ms (struct timespec *t1, struct timespec *t2) {
	return (((t1->tv_sec - t2->tv_sec) * 1000) + ((t1->tv_nsec - t2->tv_nsec) / 1000000));
} /* interval_ms () */
	
long elapsed_ms (struct timespec *ref) {
	struct timespec now;

	clock_gettime (CLOCK_BOOTTIME, &now);
	return interval_ms (&now, ref);
} /* elapsed_ms () */

/* keep calling nanosleep until full timeout, in face of EINTRs */
void sleep_ms (long ms) {
	struct timespec req, *rem;
	req.tv_sec = ms / 1000;
	req.tv_nsec = (ms % 1000) * 1000000;
	rem = &req;
	while (nanosleep (rem, rem));
	return;
} /* sleep_ms () */

char * pluralise (int arg) {
	if (1 == arg) return "";
	return "s";
} /* pluralise () */

int ms2human (long ms, char *buf, size_t len) {
	if (!buf) {
		BWARN ("buf is null");
		return -1;
	}
	if (0 >= len) {
		BWARN ("len: %ld", len);
		return -1;
	}
	char *p = buf;
	char *e = buf + len - 1;
	if (0 > ms) {
		*p++ = '-';
		ms = -ms;
	}
	long secs = ms / 1000;
	if (60 <= secs) {
		long mins = secs / 60;
		if (60 <= mins) {
			long hrs = mins / 60;
			if (24 <= hrs) {
				long days = hrs / 24;
				if (365 <= days) {
					// use the Gregorian Calendar definition of how often leap
					// years occur, bumped by half a day
					long yrs = (days * 10000 + 5000) / 3652425;
					if (yrs) {
						p += snprintf (p, e - p, "%ld year%s ", yrs, pluralise (yrs));
					}
				}
				days = ((days * 10000 + 5000) % 3652425) / 10000;
				if (days || (p > buf)) {
					p += snprintf (p, e - p, "%ld day%s ", days, pluralise (days));
				}
			}
			hrs = hrs % 24;
			if (hrs || (p > buf)) {
				p += snprintf (p, e - p, "%ld hr%s ", hrs, pluralise (hrs));
			}
		}
		mins = mins % 60;
		if (mins || (p > buf)) {
			p += snprintf (p, e - p, "%ld min%s ", mins, pluralise (mins));
		}
	}
	secs = secs % 60;
	p += snprintf (p, e - p, "%ld.%03ld sec%s", secs, ms % 1000, pluralise (secs));
	return (p - buf);
} /* ms2human () */

int bccCalc (char *buf, int len) {
	char bcc = 0;
	while (len--) bcc ^= *buf++;
	return bcc;
} /* bccCalc () */

int checkPIDFile (char *name) {
	int other_pid = 0;
	char buf[256];
	int fd = open (name, O_RDONLY);
	if (fd >= 0) {
		int rv = read (fd, buf, sizeof (buf) - 1);
		if (rv > 0) {
			buf[rv] = '\0';
			other_pid = atoi (buf);
		}
		close (fd);
	}
	if (other_pid > 0) {
		struct stat statbuf;
		snprintf (buf, sizeof (buf), "/proc/%d", other_pid);
		fd = stat (buf, &statbuf);
		BDEBUG ("stat %s returned %d", buf, fd);
		if (fd == 0) {
			return other_pid;
		}
	}
	return 0;
} /* checkPIDFile () */

int writePIDFile (char *format, ...) {
	char name[256];
	va_list ap;
	va_start (ap, format);
	vsnprintf (name, sizeof (name) - 1, format, ap);
	va_end (ap);
	int fd = creat (name, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd < 0) {
		BDEBUG ("unable to open PID file %s", name);
		return -1;
	}
	dprintf (fd, "%d", getpid ());
	close (fd);
	return 0;
} /* writePIDFile () */

#ifdef TESTING
void test_word_count () {
	char *str1 = "It was a dark and stormy	 night";
	char *str2 = "  It 	\n was a dark and stormy	 night\n";

	assert (0 == word_count (NULL));
	assert (0 == word_count (""));
	assert (0 == word_count (" \t\n\r\f\v"));
	assert (7 == word_count (str1));
	assert (7 == word_count (str2));
} /* test_word_count () */
#endif /* TESTING */

int word_count (const char *t) {
	int rv = 0;
	if (!t) return 0;
	int inw = 0;
	while (*t) {
		if (isspace (*t++)) {
			inw = 0;
		} else {
			if (!inw) rv++;
			inw = 1;
		}
	}
	return rv;
} /* word_count () */

#ifdef TESTING
void test_parse_args () {
	char *args1 = "hello";
	char *args2 = " hello\tthere \n";
	char *args3 = "test -f /etc/foobar\n";
	char *argv[20];
	char args[256];

	assert (-1 == parse_args (NULL, 1, strcpy (args, args1)));
	assert (-1 == parse_args (argv, 0, strcpy (args, args1)));
	assert (-1 == parse_args (argv, 1, NULL));
	assert (0 == parse_args (argv, 1, strcpy (args, "")));
	assert (NULL == argv[0]);
	assert (0 == parse_args (argv, 1, strcpy (args, "\n")));
	assert (NULL == argv[0]);
	assert (1 == parse_args (argv, 1, strcpy (args, args1)));
	assert (NULL == argv[0]);
	argv[1] = "help";
	assert (1 == parse_args (argv, 2, strcpy (args, args1)));
	assert (!strcmp (argv[0], "hello"));
	assert (NULL == argv[1]);
	assert (1 == parse_args (argv, 2, strcpy (args, "  hello  ")));
	assert (!strcmp (argv[0], "hello"));
	assert (NULL == argv[1]);
	assert (2 == parse_args (argv, 2, strcpy (args, args2)));
	assert (!strcmp (argv[0], "hello\tthere"));
	assert (NULL == argv[1]);
	assert (2 == parse_args (argv, 3, strcpy (args, args2)));
	assert (!strcmp (argv[0], "hello"));
	assert (!strcmp (argv[1], "there"));
	assert (NULL == argv[2]);
	assert (3 == parse_args (argv, 3, strcpy (args, args3)));
	assert (!strcmp (argv[0], "test"));
	assert (!strcmp (argv[1], "-f /etc/foobar"));
	assert (NULL == argv[2]);
	assert (3 == parse_args (argv, 5, strcpy (args, args3)));
	assert (!strcmp (argv[0], "test"));
	assert (!strcmp (argv[1], "-f"));
	assert (NULL == argv[3]);
} /* test_parse_args () */
#endif /* TESTING */

int parse_args (char *argv[], int numv, char *args) {
	if (!argv || (numv < 1) || !args) {
		return -1;
	}
	numv--;
	int rv = 0;
	int inword = 0;
	char *endp = NULL;
	while (*args) {
		if (isspace (*args)) {
			if (inword) endp = args;
			inword = 0;
		} else {
			if (!inword) {
				if (numv) {
					*argv++ = args;
					if (endp && numv) *endp = '\0';
					numv--;
				}
				endp = NULL;
				rv++;
			}
			inword = 1;
		}
		args++;
	}
	if (endp) *endp = '\0';
	*argv = NULL;
	return rv;
} /* parse_args () */

#ifdef TESTING
void test_find_arg () {
	char *args1[] = {
		"test",
		"-f",
		"boo",
		NULL,
	};
	char *args2 = "test bar -f=foo -csix -q bart\n";
	int argc;
	char *argv[20];
	char args[256];

	assert (NULL == find_arg (0, NULL, NULL));
	assert (NULL == find_arg (0, args1, "-f"));
	assert (!strcmp ("boo", find_arg (3, args1, "-f")));
	assert (NULL == find_arg (3, args1, "-c"));
	argc = parse_args (argv, 20, strcpy (args, args2));
	assert (!strcmp ("foo", find_arg (argc, argv, "-f")));
	assert (!strcmp ("six", find_arg (argc, argv, "-c")));
	assert (!strcmp ("bart", find_arg (argc, argv, "-q")));
	assert (!strcmp ("r", find_arg (argc, argv, "ba")));
	assert (!strcmp ("", find_arg (argc, argv, "bart")));
} /* test_find_arg () */
#endif /* TESTING */

char *find_arg (int argc, char *argv[], char *t) {
	char *rv = NULL;
	if (!argv || !t) return rv;
	size_t len = strlen (t);
	for (int i = 1; i < argc; i++) {
		if (!strncmp (argv[i], t, len)) {
			if (strlen (argv[i]) == len) {
				if (i < argc - 1) {
					rv = argv[i + 1];
				} else {
					rv = "";
				}
			} else if (argv[i][len] == '=') {
				rv = &argv[i][len + 1];
			} else {
				rv = &argv[i][len];
			}
			break;
		}
	}
	return rv;
} /* find_arg () */

#ifdef TESTING
void test_strncpychr () {
	char res[100];
	assert (NULL == strncpychr (NULL, NULL, 0, 0));
	assert (NULL == strncpychr (NULL, teststrs[0], 0, 0));
	assert (NULL == strncpychr (NULL, teststrs[0], -2, 0));
	assert (!strcmp ("Hello", strncpychr (res, teststrs[0], 100, ' ')));
	assert (!strcmp ("Hell", strncpychr (res, teststrs[0], 5, ' ')));
	assert (!strcmp ("Hell", strncpychr (res, teststrs[0], 10, 'o')));
} /* test_strncpychr () */
#endif /* TESTING */

char *strncpychr (char *dest, const char *src, size_t n, char delim) {
	if (!dest || !src || (n < 1)) return NULL;
	char *rv = dest;
	while (*src && (*src != delim) && --n) {
		*dest++ = *src++;
	}
	*dest = '\0';
	return rv;
} /* strncpychr () */ 

void fHexDump (FILE *f, const void *buf, int len) {
	char ob [100];
	char ascii [20];
	const unsigned char *b = buf;

	ascii [16] = 0;
	while (len > 0) {
		char *p = ob;
		p += sprintf (p, "%05x  ", (unsigned int)((void *)b - buf));
		for (int n = 0; n < 16; n++) {
			if (len) {
				ascii [n] = isgraph (*b) ? *b : '.';
				p += sprintf (p, "%02x ", *b++);
				len--;
			} else {
				ascii [n] = ' ';
				p += sprintf (p, "   ");
			}
		}
		fprintf (f, "%s %s\n", ob, ascii);
	}
} /* fHexDump () */

void fprintFeatureTest (FILE *f) {
#ifdef _POSIX_SOURCE
	fprintf (f, "_POSIX_SOURCE defined\n");
#endif

#ifdef _POSIX_C_SOURCE
	fprintf (f, "_POSIX_C_SOURCE defined: %ldL\n", (long) _POSIX_C_SOURCE);
#endif

#ifdef _ISOC99_SOURCE
	fprintf (f, "_ISOC99_SOURCE defined\n");
#endif

#ifdef _ISOC11_SOURCE
	fprintf (f, "_ISOC11_SOURCE defined\n");
#endif

#ifdef _XOPEN_SOURCE
	fprintf (f, "_XOPEN_SOURCE defined: %d\n", _XOPEN_SOURCE);
#endif

#ifdef _XOPEN_SOURCE_EXTENDED
	fprintf (f, "_XOPEN_SOURCE_EXTENDED defined\n");
#endif

#ifdef _LARGEFILE64_SOURCE
	fprintf (f, "_LARGEFILE64_SOURCE defined\n");
#endif

#ifdef _FILE_OFFSET_BITS
	fprintf (f, "_FILE_OFFSET_BITS defined: %d\n", _FILE_OFFSET_BITS);
#endif

#ifdef _BSD_SOURCE
	fprintf (f, "_BSD_SOURCE defined\n");
#endif

#ifdef _SVID_SOURCE
	fprintf (f, "_SVID_SOURCE defined\n");
#endif

#ifdef _DEFAULT_SOURCE
	fprintf (f, "_DEFAULT_SOURCE defined\n");
#endif

#ifdef _ATFILE_SOURCE
	fprintf (f, "_ATFILE_SOURCE defined\n");
#endif

#ifdef _GNU_SOURCE
	fprintf (f, "_GNU_SOURCE defined\n");
#endif

#ifdef _REENTRANT
	fprintf (f, "_REENTRANT defined\n");
#endif

#ifdef _THREAD_SAFE
	fprintf (f, "_THREAD_SAFE defined\n");
#endif

#ifdef _FORTIFY_SOURCE
	fprintf (f, "_FORTIFY_SOURCE defined\n");
#endif
} /* fprintfFeatureTest () */

/*
 * some versions of glibc don't have twalk_r (), so built my own...
 */
void tree_walk (
	const struct tree_node *this,
	void (*action) (void *, void *),
	void *closure
) {
	const struct tree_node *left;
	left = (struct tree_node *)(this->left_node & ~((uintptr_t) 0x1));
	if (left) {
		tree_walk (left, action, closure);
	}
	(*action) (this->key, closure);
	if (this->right_node) {
		tree_walk (this->right_node, action, closure);
	}
} /* tree_walk () */

/* recursively dump a tree. If key is a string, print it, else show pointer */
void tree_dump (const struct tree_node *this, int level, FILE *fp, int is_str) {
	if (!this) return;
	const struct tree_node *left;
	left = (struct tree_node *)(this->left_node & ~((uintptr_t) 0x1));
	char buf[100];
	char *p = buf;
	*p = 0;
	for (int i = 0; i < level; i++) {
		*p++ = ' ';
		*p++ = ' ';
		*p = 0;
	}
	if (left) {
		fprintf (fp, "%sL:%p\n", buf, left);
		tree_dump (left, level + 1, fp, is_str);
	}
	if (is_str) {
		fprintf (fp, "%sK:'%s'\n", buf, (char *)this->key);
	} else {
		fprintf (fp, "%sK:%p\n", buf, this->key);
	}
	if (this->right_node) {
		fprintf (fp, "%sR:%p\n", buf, this->right_node);
		tree_dump (this->right_node, level + 1, fp, is_str);
	}
} /* tree_dump () */

/* recursively delete a tree */
/* note: free's the key, but if the key is a struct with other alloc'd
 *  items, they will become orphaned... use tdestroy instead... */
void tree_deleter (struct tree_node *this) {
	if (!this) return;
	struct tree_node *left;
	left = (struct tree_node *)(this->left_node & ~((uintptr_t) 0x1));
	if (left) {
		tree_deleter (left);
	}
	if (this->right_node) {
		tree_deleter (this->right_node);
	}
	free ((void *)this->key);
	free ((void *)this);
} /* tree_deleter () */

void tree_collector (void *key, void *lp) {
	struct tree_node_list *list = (struct tree_node_list *)lp;
	if (list->pos < list->max) {
		BDEBUG ("about to add %p to list at pos %d", key, list->pos);
		list->list[list->pos] = key;
	}
	list->pos++;
} /* tree_collector () */

size_t get_tree_nodes (void ***nodes, const void *tree, int num) {
	struct tree_node_list list;
	if (!tree || !num) {
		return -1;
	}
	list.pos = 0;
	list.max = num;
	if (!*nodes) {
		*nodes = calloc (num + 1, sizeof (void *));
		if (!*nodes) {
			BCRIT ("calloc");
			return -1;
		}
	}
	list.list = *nodes;
	tree_walk ((const struct tree_node *)tree, tree_collector, &list);
	return list.pos;
} /* get_tree_nodes () */

#if TESTING
void test_get_quoted_strings () {
	char *test_str = "\"Hello World!\n\", \"Hello\", \"World\", \" \",";
	char *res[10];

	assert (0 == get_quoted_strings (NULL, NULL, 0));
	assert (0 == get_quoted_strings (res, NULL, 0));
	assert (0 == get_quoted_strings (NULL, test_str, 0));
	assert (0 == get_quoted_strings (res, test_str, -1));
	char *t1 = strdup (test_str);
	assert (4 == get_quoted_strings (res, t1, 10));
	strcpy (t1, test_str);
	assert (4 == get_quoted_strings (res, t1, 4));
	strcpy (t1, test_str);
	assert (3 == get_quoted_strings (res, t1, 3));
	free (t1);
} /* test_get_quoted_strings () */
#endif /* TESTING */

int get_quoted_strings (char **out, char *in, int num) {
	int rv = 0;
	if (!out || !in || num <= 0) {
		return 0;
	}
	while (rv < num && *in) {
		while (*in && *in != '"') in++;
		if (*in++ == '"') {
			if (*in) {
				char *at = in;
				while (*in && *in != '"') in++;
				if (*in == '"') {
					*in = 0;
					in++;
					*out = at;
					out++;
					rv++;
				}
			}
		}
	}
	return rv;
} /* get_quoted_strings () */

size_t strStrLen (char *sstr) {
	if (!sstr) return 0;
	size_t len = 0;
	while (*sstr) {
		size_t slen = strlen (sstr) + 1;
		len += slen;
		sstr += slen;
	}
	return len;
} /* strStrLen () */

char *strStrDup (char *sstr) {
	size_t len = strStrLen (sstr);
	if (!len) return NULL;
	char *dst = malloc (len + 1);
	if (dst) {
		memcpy (dst, sstr, len + 1);
	}
	return dst;
} /* strStrDup () */

#ifdef TESTING
void test_strStrNum () {
	char *rv;
	char buf[1024];

	assert (0 == strStrNum (words2strStr (buf, "")));
	assert (1 == strStrNum (words2strStr (buf, "-1")));
	assert (3 == strStrNum (words2strStr (buf, "people place nations")));
	rv = words2strStr (NULL, "people place nations ");
	assert (3 == strStrNum (rv));
	free (rv);
	assert (4 == strStrNum (words2strStr (buf, "peo ple place nations  ")));
} /* test_strStrNum () */
#endif

int strStrNum (char *sstr) {
	int rv = 0;
	for (char *i = sstr; *i; i += strlen (i) + 1) rv++;
	return rv;
} /* strStrNum () */

/* convert a string of strings to a fully malloc'd array of strings */
char **strStr2strArr (char *sstr) {
	char **strArr = (char **) calloc (strStrNum (sstr) + 1, sizeof (char *));
	if (!strArr) {
		BCRIT ("calloc");
		return NULL;
	}
	char **strp = strArr;
	while (*sstr) {
		BDEBP1 ("doing '%s'", sstr);
		*strp = strdup (sstr);
		if (!*strp) {
			for (strp = strArr; *strp; strp++) free (*strp);
			free (strArr);
			BCRIT ("strdup");
			return NULL;
		}
		strp++;
		sstr += strlen (sstr) + 1;
	}
	*strp = NULL;
	return strArr;
} /* strStr2strArr () */

#if TESTING
void test_words2strStr () {
	char *rv;
	char *c;
	char buf[1024];

	rv = words2strStr (NULL, "");
	assert (NULL != rv);
	assert (0 == strlen (rv));
	free (rv);
	rv = words2strStr (NULL, "people place nations");
	assert (NULL != rv);
	assert (21 == strStrLen (rv));
	c = rv;
	assert (6 == strlen (c));
	c += strlen (c) + 1;
	assert (5 == strlen (c));
	c += strlen (c) + 1;
	assert (7 == strlen (c));
	c += strlen (c) + 1;
	assert (0 == *c);
	free (rv);
	rv = words2strStr (NULL, "people   place nations");
	assert (21 == strStrLen (rv));
	c = rv;
	c += strlen (c) + 1;
	assert (5 == strlen (c));
	free (rv);
	memset (buf, 0x55, sizeof (buf));
	rv = words2strStr (buf, "people   place nationals  ");
	assert (23 == strStrLen (rv));
	c = rv;
	c += strlen (c) + 1;
	c += strlen (c) + 1;
	assert (9 == strlen (c));
}
#endif /* TESTING */

/* convert string of words to string of strings, allocating space */
char *words2strStr (char *sstr, const char *words) {
	if (!sstr) {
		if (!(sstr = malloc (strlen (words) + 1))) {	/* worst case len */
			BCRIT ("malloc");
			return NULL;
		}
	}
	char *c = sstr;
	int in_word = 0;
	for (; *words; words++) {
		if (isgraph (*words)) {
			*c++ = *words;
			in_word = 1;
		} else if (in_word) {
			*c++ = 0;
			in_word = 0;
		}
	}
	if (in_word) *c++ = 0;		/* terminate last string */
	*c = 0;						/* terminate string-of-strings */
	return sstr;
} /* words2strStr () */

/* note: getenv () is NOT thread-safe - only run this in init code */
int is_start_from_systemd () {
	return NULL != getenv ("INVOCATION_ID");
} /* is_start_from_systemd () */

#if TESTING
void test_shortmon2int () {
	assert (-1 == shortmon2int (NULL));
	assert (-1 == shortmon2int (""));
	assert (-1 == shortmon2int ("hello"));
	assert (0 == shortmon2int ("Jan"));
	assert (0 == shortmon2int ("jan"));
	assert (0 == shortmon2int ("January"));
	assert (0 == shortmon2int ("Janus is my friend"));
	assert (11 == shortmon2int ("dec"));
} /* test_shortmon2int () */
#endif /* TESTING */

int shortmon2int (char *month) {
	if (!month) return -1;
	for (int i = 0; month_names[i]; i++) {
		if (!strncasecmp (month, month_names[i], 3)) return i;
	}
	return -1;
} /* shortmon2int () */

/* use mmap to copy a file between two open fds */
int mmap_copy (int dfd, int sfd, size_t size) {
	int retval = -1;
	char *sp, *dp;

	ftruncate (dfd, size);

	sp = mmap (NULL, size, PROT_READ, MAP_PRIVATE, sfd, 0);
	dp = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, dfd, 0);

	memcpy (dp, sp, size);
	retval = 0;

	munmap (dp, size);
	munmap (sp, size);

	return retval;
} /* mmap_copy () */

/* copy a file, not preserving modes, owner or atime/mtime */
int file_copy (char *dest, char *src) {
	int retval = -1;
	int sfd = -1;
	int dfd = -1;
	struct stat stat_buf;

	do {
		sfd = open (src, O_RDONLY);
		if (0 > sfd) {
			BERR ("open %s", src);
			break;
		}
		if (0 > fstat (sfd, &stat_buf)) {
			BERR ("stat on %s", src);
			break;
		}

		dfd = open (dest, O_RDWR | O_CREAT, 0600);
		if (0 > dfd) {
			BERR ("open %s", dest);
			break;
		}

		retval = mmap_copy (dfd, sfd, stat_buf.st_size);
		if (0 > retval) {
			BERR ("mmap_copy");
			break;
		}
	} while (0);
	if (0 >= dfd) close (dfd);
	if (0 >= sfd) close (sfd);
	return retval;
} /* file_copy () */

/* copy a symlink */
int symlink_copy (char *dest, char *src) {
	char target[PATH_MAX];
	size_t linklen = readlink (src, target, sizeof (target));
	if (0 > linklen) {
		BERR ("readlink %s", src);
		return -1;
	}
	if (PATH_MAX <= linklen) {
		BERR ("readlink %s, len exceeds PATH_LEN (%d)", src, PATH_MAX);
		return -1;
	}
	if (symlink (target, dest)) {
		BERR ("symlink of %s to %s", dest, target);
		return -1;
	}
	return 0;
} /* symlink_copy () */

/* recursively copy a file tree */
int recursive_copy (char *dest, char *src, int flags, uid_t uid, gid_t gid, int level) {
	int retval = -1;
	char nsrc[PATH_MAX];
	char ndest[PATH_MAX];
	struct stat dest_stat;
	DIR *dir_stream = NULL;
	int src_fd = -1;
	int dest_fd = -1;

	if (!dest || !src || !*dest || !*src) {
		BERR ("missing src or dest");
		return retval;
	}

	if (level > 100) {
		BERR ("too many directory levels");
		return retval;
	}

	do {
		dir_stream = opendir (src);
		if (!dir_stream) {
			BERR ("opendir on %s", src);
			break;
		}
		src_fd = dirfd (dir_stream);
		if (0 > src_fd) {
			BERR ("dirfd on %s", src);
			break;
		}

		if (0 > lstat (dest, &dest_stat)) {
			BERR ("lstat on %s", dest);
			break;
		}

		if (!S_ISDIR(dest_stat.st_mode)) {
			BERR ("%s not a dir", dest);
			break;
		}

		dest_fd = open (dest, O_RDONLY | O_DIRECTORY);
		if (0 > dest_fd) {
			BERR ("open: '%s'", dest);
			break;
		}

		BDEBUG ("recursively copying %s to %s at level %d", src, dest, level);

		retval = 0;
		while (!retval) {
			struct stat stat_buf;
			struct dirent *dir_ent = readdir (dir_stream);
			if (!dir_ent) break;
			char *fname = dir_ent->d_name;
			if (!strcmp (".", fname) || !strcmp ("..", fname)) {
				continue;
			}
			snprintf (nsrc, PATH_MAX, "%s/%s", src, fname);
			snprintf (ndest, PATH_MAX, "%s/%s", dest, fname);
			if (DT_REG == dir_ent->d_type) {
				BDEBUG ("got a reg file: %s", fname);
				retval = file_copy (ndest, nsrc);
			} else if (DT_DIR == dir_ent->d_type) {
				if (!lstat (ndest, &stat_buf)) {
					if (!S_ISDIR(stat_buf.st_mode)) {
						BERR ("%s exists and is not a directory", ndest);
						retval = -1;
						break;
					}
				} else if (mkdir (ndest, S_IRWXU | S_IXOTH) < 0) {
					BERR ("mkdir '%s'", ndest);
					retval = -1;
					break;
				} else {
					BDEBUG ("made dir %s", ndest);
				}
				retval = recursive_copy (ndest, nsrc, flags, uid, gid, level + 1);
			} else if (DT_LNK == dir_ent->d_type) {
				BDEBUG ("got a symlink: %s", fname);
				retval = symlink_copy (ndest, nsrc);
			} else {
				BERR ("unsupported directory entry type %d for %s",
					dir_ent->d_type, fname);
				retval = -1;
				break;
			}

			if (!retval) {
				struct stat nsrc_stat;
				if (0 > lstat (nsrc, &nsrc_stat)) {
					BERR ("lstat on %s", nsrc);
					retval = -1;
					break;
				}
				if (DT_LNK != dir_ent->d_type) {
					if (fchmodat (dest_fd, fname, nsrc_stat.st_mode & 07777, 0)) {
						BERR ("fchmodat %s", fname);
						retval = -1;
						break;
					}
					if (0 > fchownat (dest_fd, fname, uid, gid, AT_SYMLINK_NOFOLLOW)) {
						BERR ("fchownat %s", fname);
						retval = -1;
						break;
					}
				}
				if (flags & PRESERVE_TIMES) {
					struct timespec times[2];
					times[0].tv_sec = nsrc_stat.st_atim.tv_sec;
					times[0].tv_nsec = nsrc_stat.st_atim.tv_nsec;
					times[1].tv_sec = nsrc_stat.st_mtim.tv_sec;
					times[1].tv_nsec = nsrc_stat.st_mtim.tv_nsec;
					if (0 > utimensat (dest_fd, fname, times, AT_SYMLINK_NOFOLLOW)) {
						BERR ("utimensat on %s", dest);
						retval = -1;
						break;
					}
				}
			}
		}
	} while (0);

	if (0 <= dest_fd) {
		close (dest_fd);
	}
	if (dir_stream) {
		if (0 > closedir (dir_stream)) {
			BERR ("closedir");
		}
	}
	return retval;
} /* recursive_copy () */
