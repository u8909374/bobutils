/*
 * testutil - unit tests on functions in util.c
 *
 * by Robert (bob) Edwards, July 2021
 *
 * compile with: gcc -Wall testutil.c util.c
 *
 * GPLv2 etc.
 */

#include "util.h"
#include "version.h"
#include <search.h>
#include <limits.h>

char *teststr[] = {
	"Hello World!\n",
	"Hello",
    "World",
	" ",
	NULL
};

void test_strAppend () {
	char buf[1024];
	char *str1 = teststr[0];
	char str2[] = " - ";
	char str3[] = "fred";
	int len;

	strcpy (buf, str1);
	strAppend (buf, str2, str3);
	len = strlen (buf);
	assert (len == strlen (str1) + strlen (str2) + strlen (str3));
	strAppend (NULL, str2, str3);
	strcpy (buf, str1);
	strAppend (buf, NULL, str3);
	len = strlen (buf);
	assert (len == strlen (str1) + strlen (str3));
	strcpy (buf, str1);
	strAppend (buf, "", str3);
	len = strlen (buf);
	assert (len == strlen (str1) + strlen (str3));
	strcpy (buf, str1);
	strAppend (buf, str2, NULL);
	len = strlen (buf);
	assert (len == strlen (str1) + strlen (str2));
	strcpy (buf, str1);
	strAppend (buf, str2, "");
	len = strlen (buf);
	assert (len == strlen (str1) + strlen (str2));
} /* test_strAppend () */

void test_myStrCat () {
	char buf[1024];
	char *str1 = "hello ";
	char *str2 = "world!\n";

	strcpy (buf, str1);
	assert (!strcmp (myStrCat (buf, 50, str2), "hello world!\n"));
	assert (myStrCat (NULL, 10, str2) == NULL);
	strcpy (buf, str1);
	assert (myStrCat (buf, 0, str2) == NULL);
	assert (myStrCat (buf, 4, str2) == NULL);
	assert (myStrCat (buf, 6, str2) == NULL);
	assert (!strcmp (myStrCat (buf, 7, NULL), "hello "));
	strcpy (buf, str1);
	assert (!strcmp (myStrCat (buf, 7, str2), "hello "));
	strcpy (buf, str1);
	assert (!strcmp (myStrCat (buf, 8, str2), "hello w"));
} /* test_myStrCat () */

void test_str2lower () {
	char buf[1024];
	char str1[] = "hEllO w0rld!\n";
	char *ret;

	strcpy (buf, str1);
	ret = str2lower (buf);
	assert (strlen (ret) == strlen (str1));
	assert (!strcmp ("hello w0rld!\n", ret));
	ret = str2lower (NULL);
	assert (ret == NULL);
	ret = str2lower ("");
	assert (strlen (ret) == 0);
} /* test_str2lower () */

void test_hex2num () {
	assert (hex2num ('0') == 0);
	assert (hex2num (0) == -1);
	assert (hex2num ('B') == 11);
	assert (hex2num ('e') == 14);
	assert (hex2num ('h') == -1);
	assert (hex2num2 ("00") == 0);
	assert (hex2num2 ("09") == 9);
	assert (hex2num2 ("0f") == 15);
	assert (hex2num2 ("fF") == 255);
	assert (hex2num2 ("xf") == -1);
	assert (hex2num2 ("0g") == -1);
} /* test_hex2num () */

void test_strhex2bin () {
	unsigned char buf[1024];

	memset (buf, 0, sizeof (buf));
	assert (4 == strhex2bin (buf, "deadBeef", 8));
	assert (0xbe == buf[2]);
	assert (0 == buf[4]);
	assert (-1 == strhex2bin (NULL, "c", 2));
	assert (-1 == strhex2bin (buf, NULL, 2));
	assert (0 == strhex2bin (buf, "c", 0));
	assert (1 == strhex2bin (buf, "c", 1));
	assert (12 == buf[0]);
	buf[0] = 0;
	assert (0 == strhex2bin (buf, "c", 2));
	assert (1 == strhex2bin (buf, "0c", 2));
	assert (12 == buf[0]);
	assert (-1 == strhex2bin (buf, "x", 2));
} /* test_strhex2bin () */

void test_str2Cs () {
	char buf[1024];
	char str1[] = "hello\n";
	char str2[] = "\02";
	char str3[] = "d\377\n";

	assert (strlen (str2Cs (buf, sizeof (buf) - 1, str1)) == strlen (str1) + 1);
	assert (!strcmp (buf, "hello\\n"));
	assert (NULL == str2Cs (NULL, sizeof (buf) - 1, str1));
	assert (NULL == str2Cs (buf, 0, str1));
	assert (NULL == str2Cs (buf, sizeof (buf) - 1, 0));
	assert (0 == strlen (str2Cs (buf, sizeof (buf) - 1, "")));
	strcpy (buf, str1);
	assert (3 == strlen (str2Cs (buf, 4, str1)));
	assert (4 == strlen (str2Cs (buf, 5, str2)));
	assert (0 == strlen (str2Cs (buf, 3, str2)));
	assert (7 == strlen (str2Cs (buf, 8, str3)));
//	printf ("'%s' (len: %ld)\n", buf, strlen (buf));
	assert (1 == strlen (str2Cs (buf, 4, str3)));
} /* test_str2cs () */

void test_bccCalc () {
	char *t;
	assert (0 == bccCalc (NULL, 0));
	assert (0 == bccCalc ("\0", 1));
	assert (0 == bccCalc ("\0", 2));
	/* p11 E905 External Controls manual */
	t = "\0010A0E0A\00200100064\003xx";
	assert (0x77 == bccCalc (&t[1], 16));
} /* test_bccCalc () */

void test_arrCount () {
	assert (4 == arrCount (teststr));
	assert (2 == arrCount (&teststr[2]));
	assert (0 == arrCount (&teststr[4]));
	assert (0 == arrCount (NULL));
} /* test_arrCount () */

int my_strcmp (const void *l, const void *r) {
	return strcmp ((const char *)l, (const char *)r);
} /* my_strcmp () */

void test_tree_2 () {
	void *t1 = NULL;
	char **x;
//	struct tree_node t2;

	for (char **t = teststr; *t; t++) {
		x = tsearch (*t, &t1, my_strcmp);
		assert (*x == *t);
	}
	assert (t1);
	char *y = strdup (teststr[1]);
	x = tsearch (teststr[1], &t1, my_strcmp);
	assert (*x != y);
	x = tdelete (y, &t1, my_strcmp);
	assert (*x != y);
	x = tfind (y, &t1, my_strcmp);
	assert (!x);
} /* test_tree () */

void test_ms2human () {
	char buf[1000];

	assert (-1 == ms2human (0, NULL, 1));
	assert (-1 == ms2human (0, buf, 0));
	assert (10 == ms2human (0, buf, sizeof (buf)));
	assert (!strcmp (buf, "0.000 secs"));
	ms2human (1, buf, sizeof (buf));
	assert (!strcmp (buf, "0.001 secs"));
	ms2human (999, buf, sizeof (buf));
	assert (!strcmp (buf, "0.999 secs"));
	ms2human (1003, buf, sizeof (buf));
	assert (!strcmp (buf, "1.003 sec"));
	ms2human (43210, buf, sizeof (buf));
	assert (!strcmp (buf, "43.210 secs"));
	ms2human (543210, buf, sizeof (buf));
	assert (!strcmp (buf, "9 mins 3.210 secs"));
	ms2human (6543210, buf, sizeof (buf));
	assert (!strcmp (buf, "1 hr 49 mins 3.210 secs"));
	ms2human (76543210, buf, sizeof (buf));
	assert (!strcmp (buf, "21 hrs 15 mins 43.210 secs"));
	ms2human (876543210, buf, sizeof (buf));
	assert (!strcmp (buf, "10 days 3 hrs 29 mins 3.210 secs"));
	ms2human (9876543210, buf, sizeof (buf));
	assert (!strcmp (buf, "114 days 7 hrs 29 mins 3.210 secs"));
	ms2human (-43210, buf, sizeof (buf));
	assert (!strcmp (buf, "-43.210 secs"));
	long ms_in_day = 24 * 60 * 60000;
	ms2human ((long) (365) * ms_in_day, buf, sizeof (buf));
	assert (!strcmp (buf, "1 year 0 days 0 hrs 0 mins 0.000 secs"));
	ms2human ((long) (2 * 365) * ms_in_day, buf, sizeof (buf));
	assert (!strcmp (buf, "2 years 0 days 0 hrs 0 mins 0.000 secs"));
	ms2human ((long) (3 * 365) * ms_in_day, buf, sizeof (buf));
	assert (!strcmp (buf, "2 years 365 days 0 hrs 0 mins 0.000 secs"));
	ms2human ((long) (4 * 365 + 1) * ms_in_day, buf, sizeof (buf));
	assert (!strcmp (buf, "4 years 0 days 0 hrs 0 mins 0.000 secs"));
	/* Wikipedia states that the Gregorian Calendar defines 97/400 years
	 * as leap years */
	ms2human ((long) (400 * 365 + 97) * ms_in_day, buf, sizeof (buf));
	assert (!strcmp (buf, "400 years 0 days 0 hrs 0 mins 0.000 secs"));
	/* check if this is still true 100000 time more...*/
	ms2human ((long) (40000000 * (long) 365 + 9700000) * ms_in_day, buf, sizeof (buf));
	assert (!strcmp (buf, "40000000 years 0 days 0 hrs 0 mins 0.000 secs"));
	// printf ("%s\n", buf);
	ms2human (LONG_MAX, buf, sizeof (buf));
	assert (!strcmp (buf, "292277024 years 229 days 7 hrs 12 mins 55.807 secs"));
} /* test_ms2human () */

int main (int argc, char *argv[]) {
	if ((argc > 1) && !strcmp ("-V", argv[1])) {
		printf ("Version: %s, built: %s\n", BUILD_GIT_SHA, BUILD_DATE);
		exit (EXIT_SUCCESS);
	}
	test_strAppend ();
	test_myStrCat ();
	test_str2lower ();
	test_hex2num ();
	test_strhex2bin (); 
	test_str2Cs ();
	test_bccCalc ();
	test_arrCount ();
	test_word_count ();
	test_parse_args ();
	test_find_arg ();
	test_strncpychr ();
	test_get_quoted_strings ();
	test_tree_2 ();
	test_strStrNum ();
	test_words2strStr ();
	test_shortmon2int ();
	test_ms2human ();
	printf ("all good!\n");
	return 0;
}
