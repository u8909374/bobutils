/*
 * bdbus.h - header for bob DBus library
 *
 * by Robert (bob) Edwards, August 2023
 *
 * GPLv2 etc.
 */

#ifndef _bdbus_h_
#define _bdbus_h_

#include <systemd/sd-bus.h>
#include <systemd/sd-daemon.h>

#include "util.h"

struct bdbus_disp_node {
	pthread_t tid;
	sd_bus *bus;
};

int bdbus_init (char *bus_path, const sd_bus_vtable *vtable);

#endif
