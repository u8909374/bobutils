/*
 * netlib.c - some network functions
 *
 * by Robert Edwards, April 2006
 *
 * GPLv2 etc.
 */

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h> 
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <termios.h>
#include <unistd.h>

#include "netlib.h"

#ifdef TESTING
void test_get_net_num () {
	uint8_t buf1[] = {
		0, 1, 0xfc, 4, 2, 1, 128
	};
	void *p = buf1;
	assert (508 == get_net_num (&p, 3));
	assert (1026 == get_net_num (&p, 2));
	assert (1 == get_net_num (&p, 1));
	assert (0 == get_net_num (&p, 0));
	assert (0x80 == get_net_num (&p, 1));
} /* test_get_net_num () */
#endif

/* get next len bytes as number in network byte order */
uint32_t get_net_num (void **p, int len) {
	uint32_t rv = 0;
	while (len--) {
		uint8_t v = **(uint8_t **)p;
		rv = (rv * 256) + v;
		(*p)++;
	}
	return rv;
} /* get_net_num () */

int readsome (int fd, char *buf, int len) {
	int nread;
	do {
		nread = read (fd, buf, len);
	} while (-1 == nread && EINTR == errno);
	return nread;
} /* readsome () */

int writeall (int fd, char *buf, int len) {
	int tot = 0;
	while (len > tot) {
		int nwrote = write (fd, buf + tot, len - tot);
		if (-1 == nwrote && EINTR == errno) {
			continue;
		}
		if (0 >= nwrote) {
			return -1;
		}
		tot += nwrote;
	}
	return tot;
} /* writeall () */

int readwrite (int in, int out) {
	char buf[8192];
	int nread = readsome (in, buf, sizeof (buf));
	if (0 < nread) {
		return writeall (out, buf, nread);
	}
	return nread;
} /* readwrite () */

/* transfer bytes between in and out until both ends close */
size_t transfer (int in, int out) {
	size_t rv = 0;
	struct pollfd pfds[2];
	pfds[0].fd = in;
	pfds[0].events = POLLIN;
	pfds[1].fd = out;
	pfds[1].events = POLLIN;
	int timeout = -1;
	int nopen = 2;
	while (0 < nopen) {
		int ready = poll (pfds, 2, timeout);
		BDEBUG ("poll returned %d", ready);
		if (-1 == ready && EINTR == errno) continue;
		if (0 >= ready) return rv;
		int dir = (pfds[1].revents & POLLIN) == POLLIN;
		int nread = readwrite (pfds[dir].fd, pfds[!dir].fd);
		if (0 >= nread) {
			BDEBUG ("shutting down %d for WR", dir ? in : out);
			shutdown (dir ? in : out, SHUT_WR);
			pfds[dir].fd = -1;
			nopen--;
			timeout = 10000;
			continue;
		}
		BDEBUG ("%d bytes from %d to %d", nread, pfds[dir].fd, pfds[!dir].fd);
		rv += nread;
	}
	return rv;
} /* transfer () */

/*
 * if write () fails, reconnect to a connected socket and try
 * again. note: requires ignoring SIGPIPE to prevent program
 * termination.
 */
int write_reconnect (int *fd, char *buf, int len, int ms) {
	if (*fd < 0) return -1;
	int nWrite = write (*fd, buf, len);
	if ((nWrite == -1) && (errno == EPIPE)) {
		struct sockaddr_storage ss;
		socklen_t addrlen = sizeof (ss);
		struct sockaddr *sa = SIN(&ss);
		int rv = getpeername (*fd, sa, &addrlen);
		close (*fd);
		if (rv < 0) {
			/* probably not a socket - can't do anything... */
			*fd = -1;
		} else {
			BDEBUGSA (sa);
			*fd = socket (sa->sa_family, SOCK_STREAM, 0);
			if (*fd < 0) {
				BDEBUG ("socket err");
				return -1;
			}
			BDEBUG ("fd: %d", *fd);
			if (ms > 0) {
				struct timeval timeo;
				timeo.tv_sec  = ms / 1000;
				timeo.tv_usec = (ms % 1000) * 1000;
				setsockopt (*fd, SOL_SOCKET, SO_SNDTIMEO, &timeo, sizeof (timeo));
			}
			if (connect (*fd, sa, addrlen) < 0) {
				BDEBUG ("connect err");
				close (*fd);
				*fd = -1;
				return -1;
			}
			nWrite = write (*fd, buf, len);
		}
	}
	if (nWrite < 0) {
		BDEBUG ("write err");
	}
	return nWrite;
} /* write_reconnect () */

int inSockAddr (struct sockaddr *sa, int port, void *addr) {
	struct sockaddr_in *sin = SIN4(sa);

	memset ((void *) sin, 0, sizeof (struct sockaddr_in));
	sin->sin_family = AF_INET;
	sin->sin_port = htons(port);
	memcpy (&sin->sin_addr, addr, 4);
	return (0);
} /* inSockAddr () */

int inSockHost (struct sockaddr *sa, int port, char *hostname) {
	struct hostent *host;

	/* look up the host name */
	if ((host = gethostbyname (hostname)) == NULL)
		return -1;

	/* set up the address */
	return (inSockAddr (sa, port, host->h_addr));
} /* inSockHost () */

int tcpConnectSA (struct sockaddr *sa) {
	int sock = -1;

	do {
		if ((sock = socket (AF_INET, SOCK_STREAM, 0)) < 0) 
			break;

		if (connect (sock, sa, sizeof (*sa)) < 0)
			break;
    
		return sock;
	} while (0);

	if (sock >= 0) close (sock);
	return -1;
} /* tcpConnectSA () */

int tcpConnect (char *remoteHost, int port) {
	struct sockaddr sa;

	/* set up the socket address */
	if (inSockHost (&sa, port, remoteHost) >= 0) {
		return (tcpConnectSA (&sa));
	}

	return -1;
} /* tcpConnect () */

int tcp4Listen (int port) {
	int sock = -1;

	do {
		struct sockaddr sa;
		int reuse_addr = 1;
		u_int32_t addr = INADDR_ANY;

		if ((sock = socket (AF_INET, SOCK_STREAM, 0)) < 0)
			break;

		if (inSockAddr (&sa, port, &addr) < 0)
			break;

		if (setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, &reuse_addr,
			sizeof(reuse_addr)) < 0)
			break;

		if (bind (sock, &sa, sizeof (sa)) < 0)
			break;

		if (listen (sock, 5) == -1)
			break;

		return sock;
	} while (0);

	if (sock >= 0) close (sock);
	return -1;
} /* tcp4Listen () */

int tcp4Connect (char *host, char *port, int ms) {
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	char tmp[256];

	if (!port) {
		if (!strchr (host, ':')) {
			BDEBUG ("no ':' in %s", host);
			return -1;
		}
		strncpy (tmp, host, sizeof (tmp) - 1);
		tmp[sizeof (tmp) - 1] = 0;
		host = tmp;
		port = strchr (host, ':');
		*port = '\0';
		port++;
		BDEBUG ("host: %s, port: %s", host, port);
	}
	memset (&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	int rv = getaddrinfo (host, port, &hints, &servinfo);
	if (rv != 0) {
		BDEBUG ("getaddrinfo: %s", gai_strerror (rv));
		return -1;
	}

	struct timeval timeo;
	if (0 < ms) {
		timeo.tv_sec  = ms / 1000;
		timeo.tv_usec = (ms % 1000) * 1000;
	}

	for (p = servinfo; p; p = p->ai_next) {
		sockfd = socket (p->ai_family, p->ai_socktype, p->ai_protocol);
		if (0 > sockfd) continue;

		if (0 < ms) {
			setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, &timeo, sizeof (timeo));
		}

		if (0 <= connect (sockfd, p->ai_addr, p->ai_addrlen)) break;

		close (sockfd);
	}

	freeaddrinfo (servinfo); // all done with this structure

	if (!p)  {
		BDEBUG ("connect: failed to bind");
		return -1;
	}

	return sockfd;
} /* tcp4Connect () */

int udpBind (int port) {
	int sock = -1;

	do {
		struct sockaddr sa;
		u_int32_t addr = INADDR_ANY;
		if (inSockAddr (&sa, port, &addr) < 0)
			break;

		if ((sock = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
			break;

		if (bind (sock, &sa, sizeof (sa)) < 0)
			break;

		return sock;
	} while (0);

	if (sock >= 0) close (sock);
	return -1;
} /* udpBind () */

int udsConnect (const char *path, int ms) {
	struct sockaddr_un addr;
	int sock;

	do {
		sock = socket (AF_UNIX, SOCK_STREAM, 0);
	} while (sock == -1 && errno == EINTR);
	if (sock == -1) {
		BDEBUG ("socket err");
		return sock;
	}

	if (ms > 0) {
		struct timeval timeo;
		timeo.tv_sec  = ms / 1000;
		timeo.tv_usec = (ms % 1000) * 1000;
		setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, &timeo, sizeof (timeo));
	}

	memset (&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy (addr.sun_path, path, sizeof(addr.sun_path) - 1);

	if (connect (sock, SIN(&addr), sizeof (addr)) < 0) {
		close (sock);
		BDEBUG ("connect err");
		return -1;
	}
	return sock;
} /* udsConnect () */

int udsListen (const char *path, mode_t mode) {
	int sock;

	if (!path || !path[0]) {
		BDEBUG ("no socket path");
		return -1;
	}

	sock = socket (AF_UNIX, SOCK_STREAM, 0);
	if (sock < 0) {
		BDEBUG ("socket");
		return sock;
	}

	do {
		struct sockaddr_un sun;
		int len = strlen (path);
		if (len >= sizeof (sun.sun_path)) {
			BDEBUG ("path too long for Unix socket");
			break;
		}
		socklen_t addrlen = sizeof (sun.sun_family) + len;
		struct sockaddr *addr = SIN(&sun);

		memset (&sun, 0, sizeof (sun));
		sun.sun_family = AF_UNIX;
		strncpy (sun.sun_path, path, sizeof (sun.sun_path) - 1);

		errno = 0;
		if (connect (sock, addr, addrlen) >= 0) {
			BWARN ("already listening");
			break;
		}

		unlink (path);

		if (bind (sock, addr, addrlen) < 0) {
			BDEBUG ("bind");
			break;
		}

		if (listen (sock, 5) < 0) {
			BDEBUG ("listen");
			break;
		}

		if (mode && chmod (path, mode)) {
			BDEBUG ("chmod");
			break;
		}
		return sock;
	} while (0);

	close (sock);
	return -1;
} /* udsListen () */

#ifdef TESTING
void test_checksum () {
	char testvec1[] = {
		0x00, 0x01, 0xf2, 0x03, 0xf4, 0xf5, 0xf6, 0xf7
	};

	assert (0xd22 == checksum (testvec1, 8));
} /* test_checksum () */
#endif

uint16_t checksum (void *buf, int len) {
	long sum = 0;
	uint16_t *p = (uint16_t *) buf;

    while (len > 1) {
        sum += *p++;
        len -= 2;
    }
    if (len > 0) {
        sum += *(uint8_t *) p;
    }

    sum = (sum >> 16) + (sum & 0xffff);
    sum += sum >> 16;

    return (uint16_t) ~sum;
} /* checksum () */

int ipv4AddrToDDN (char *str, void *buf) {
	unsigned char *b = buf;
	return sprintf (str, "%d.%d.%d.%d", b[0], b[1], b[2], b[3]);
} /* ipv4AddrToDDN () */

/*
 * convert an IPv4 host address to dotted-decimal notation
 */
int dnsLkup (char *dns, char *ddn) {
	struct hostent *host;
	
	if ((host = gethostbyname (dns)) == NULL) {
		perror ("gethostbyname");
		return -1;
	}

	if (inet_ntop (AF_INET, host->h_addr, ddn, INET_ADDRSTRLEN) == NULL) {
		perror ("inet_ntop");
		return -1;
	}
	return 0;
} /* dnsLkup () */

/*
 * convert an ascii string in dotted decimal notation to binary bytes
 */
int ddn2bin (char **ip, uint8_t *buf, int size) {
	int i = 0;

	do {
		if (**ip == '.') *ip += 1;
		*buf++ = strtol (*ip, ip, 0);
	} while ((++i < size) && (**ip == '.'));
	return i;
} /* ddn2bin () */

/*
 * add an IPv4 addr & mask pair to a list
 */
struct ipv4List *ipv4ListAdd (struct ipv4List **list, char *ip) {
	struct ipv4List *new;
	uint32_t addr = 0;
	uint32_t mask = 0;
	int maskbits;

	maskbits = ddn2bin (&ip, (uint8_t *) &addr, 4) * 8;
	if (*ip) {
		if (*ip++ == '/') {
			switch (ddn2bin (&ip, (uint8_t *) &mask, 4)) {
				case 1: maskbits = *(uint8_t *) &mask; break;
				case 4: maskbits = 0; break;
				default: syslog (LOG_ERR, "bad input"); return NULL;
			}
		} else {
			syslog (LOG_ERR, "bad input"); return NULL;
		}
	}
	if (maskbits)
		mask = htonl (~((1 << (32 - maskbits)) - 1));

	new = malloc (sizeof(struct ipv4List));
	if (new) {
		new->next = NULL;
		new->addr = addr & mask;
		new->mask = mask;
		if (*list) {
			struct ipv4List *p = *list;
			while (p->next) p = p->next;
			p->next = new;
		} else {
			*list = new;
		}
	}
	return new;
} /* ipv4ListAdd () */

/*
 * process allow and deny lists to determine if addr is allowed
 *
 * follows same semantics as Apache 2 mod_access. See:
 * http://httpd.apache.org/docs/2.0/mod/mod_access.html
 * default order (0) is deny,allow
 */
int ipv4Allow (uint32_t addr, int order, struct ipv4List *allow, struct ipv4List *deny) {
	int allowed = 0;
	int denied = 0;

	while (allow && !allowed) {
		allowed = (addr & allow->mask) == allow->addr;
		allow = allow->next;
	}
	while (deny && !denied) {
		denied = (addr & deny->mask) == deny->addr;
		deny = deny->next;
	}
	return (allowed == denied) ? !order : allowed;
} /* ipv4Allow () */

int getLocalAddr (struct ifaddrs *ifaddr, char *hostname, int len) {
	struct ifaddrs *ifap, *ifa;
	int rv = -1;

	if (!ifaddr) return -1;
	/* get IP address from an interface with a DNS entry */
	if (getifaddrs (&ifap)) {
		BDEBUG ("getifaddrs");
		return -1;
	}
	int gni_val;
	for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
		/* skip anything that isn't IPv4 */
		if (ifa->ifa_addr->sa_family != AF_INET) continue;

		/* skip loopback interface */
		if (!strcmp ("lo", ifa->ifa_name)) continue;

		gni_val = getnameinfo (ifa->ifa_addr, sizeof (struct sockaddr),
			hostname, len, NULL, 0, NI_NAMEREQD);
		if (!gni_val) {
			break;
		}
	}
	if (!*hostname) {
		BDEBUG ("getnameinfo: %s", gai_strerror (gni_val));
	} else {
		BDEBUG ("hostname: %s", hostname);
		memcpy (ifaddr, ifa, sizeof (struct ifaddrs));
		rv = 0;
	}
	freeifaddrs (ifap);
	return rv;
} /* getLocalAddr () */

char *remHostName (uint32_t IP, char *name, int len) {
	struct sockaddr saddr;
	struct sockaddr_in *sa = SIN4(&saddr);
	int rv;

	sa->sin_family = AF_INET;
	sa->sin_addr.s_addr = htonl(IP);
	rv = getnameinfo (&saddr, sizeof (saddr), name, len, NULL, 0, NI_NAMEREQD);
	if (rv == 0) {
		return name;
	}
	BDEBUG("getnameinfo: %s", gai_strerror (rv));
	return NULL;
} /* remHostName () */

/* attempt to open/connect to a filename, Unix socket or TCP socket */
int strFdOpen (char *str, int ms) {
	if (!str) return -1;
	if (!strncmp (str, "tcp:", 4)) {
		str += 4;
	} else if (!strncmp (str, "file:", 5)) {
		return open (str + 5, 0);
	} else if (!strncmp (str, "unix:", 5)) {
		return udsConnect (str + 5, ms);
	} else {
		struct stat sb;
		if (lstat (str, &sb) != -1) {
			if (S_ISREG(sb.st_mode)) {
				return open (str, 0);
			} else if (S_ISSOCK(sb.st_mode)) {
				return udsConnect (str, ms);
			}
			return -1;
		}
	}
	return tcp4Connect (str, NULL, ms);
} /* strFdOpen () */

int tcp4Probe (uint32_t addr, int port, int ms) {
	struct sockaddr_in sin4;
	struct timeval timeo;
	int ret = -1;

	int sock = socket (AF_INET, SOCK_STREAM, 0);
	if (sock < 0) return -1;
	sin4.sin_family = AF_INET;
	sin4.sin_port = htons (port);
	sin4.sin_addr.s_addr = htonl (addr);
	timeo.tv_sec  = ms / 1000;
	timeo.tv_usec = (ms % 1000) * 1000;
	if (!setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, &timeo, sizeof (timeo))) {
		ret = connect (sock, SIN(&sin4), sizeof (sin4));
		if (ret < 0) {
			BDEBUG ("connect errno: %d", errno);
			if (errno == EINPROGRESS) ret = 1;
		}
	}
	close (sock);
	return ret;
} /* tcp4Probe () */

void *tcp_syn_pkt_new (char *srcIP, int port) {
	void *buf = calloc (1, sizeof (struct pseudo_hdr) + sizeof (struct tcphdr));
	if (!buf) {
		BCRIT ("calloc");
		return NULL;
	}
	struct pseudo_hdr *phdr = (struct pseudo_hdr *) buf;
	struct tcphdr *tcph = (struct tcphdr *) (buf + sizeof (struct pseudo_hdr));
	phdr->src = inet_addr (srcIP);
	phdr->protocol = IPPROTO_TCP;
	phdr->tcp_len = htons (sizeof (struct tcphdr));
	tcph->source = htons (port);
	tcph->doff = sizeof (struct tcphdr) / 4;
	tcph->syn = 1;
	tcph->window = htons (4096);
	return buf;
} /* tcp_syn_pkt_new () */

int tcp_syn_tickle (int fd, void *buf, in_addr_t dest, int port, int ms) {
	struct pseudo_hdr *phdr = (struct pseudo_hdr *) buf;
	struct tcphdr *tcph = (struct tcphdr *) (buf + sizeof (struct pseudo_hdr));
	struct sockaddr_in sin4;
	struct pollfd pfds[1];
	char rbuf[64 * 1024];
	struct iphdr *iph = (struct iphdr *) rbuf;
	uint32_t sentseq = random ();

	pfds[0].fd = fd;
	pfds[0].events = POLLIN;

	phdr->dst = dest;
	tcph->dest = port;
	tcph->seq = htonl (sentseq);
	tcph->check = 0;
	tcph->check = checksum (buf, sizeof (struct pseudo_hdr) + sizeof (struct tcphdr));
	memset (&sin4, 0, sizeof (struct sockaddr_in));
	sin4.sin_family = AF_INET;
	sin4.sin_addr.s_addr = dest;
	int nWrite = sendto (fd, tcph, sizeof (struct tcphdr), 0, SIN(&sin4),
		sizeof (sin4));
	if (nWrite <= 0) {
		BDEBUG ("sendto");
		return -1;
	}

	int last_ms = clock_ms ();
	while (1) {
		int remain = ms - (clock_ms () - last_ms);
		BDEBUG ("remain: %d", remain);
		int rv = poll (pfds, 1, remain);
		if (rv < 0) {
			if (EINTR == errno || ENOMEM == errno || EAGAIN == errno) {
				continue;
			}
			BCRIT ("poll err");
			return -1;
		}
		if (rv == 0) {
			BDEBUG ("timeout");
			return -1;
		}

		rv = recv (fd, rbuf, sizeof (rbuf), 0);
		if (rv <= 0) {
			BCRIT ("recvfrom");
			return -1;
		}
		if ((rv < sizeof (struct iphdr) + sizeof (struct tcphdr)) ||
			(iph->protocol != IPPROTO_TCP)) {
			BDEBUG ("rv: %d, proto: %d", rv, iph->protocol);
			continue;
		}
		tcph = (struct tcphdr *) (rbuf + (iph->ihl * 4));
		// instead of checking checksum, rely on seq number correctness
		if (sentseq + 1 == ntohl (tcph->ack_seq)) {
			if (tcph->syn == 1 && tcph->ack == 1) {
				return 1;
			}
			if (tcph->rst == 1) {
				return 0;
			}
		}
		BDEBUGTCP (tcph);
	}
	return -1;
} /* tcp_syn_tickle () */

in_addr_t ipv4Netmask (in_addr_t ipv4) {
	in_addr_t netmask = -1;
	struct ifaddrs *ifaddr;

	if (getifaddrs (&ifaddr) == -1) {
		BCRIT ("getifaddrs");
		return netmask;
	}
	for (struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL) continue;
		if (AF_INET != ifa->ifa_addr->sa_family) continue;
		struct sockaddr_in *sin = SIN4(ifa->ifa_addr);
		if (ipv4 != sin->sin_addr.s_addr) continue;
		BDEBUG ("using network interface: %s", ifa->ifa_name);
		sin = SIN4(ifa->ifa_netmask);
		netmask = sin->sin_addr.s_addr;
		break;
	}
	free (ifaddr);
	return netmask;
} /* ipv4Netmask () */

void fprintSA (FILE *f, struct sockaddr *sa) {
	switch (sa->sa_family) {
		case AF_INET: do {
			struct sockaddr_in *sin = SIN4(sa);
			char addr[20];
			ipv4AddrToDDN (addr, &sin->sin_addr);
			fprintf (f, "AF_INET, host: '%s', port: %d\n",
				addr, ntohs (sin->sin_port));
			} while (0); break;
		case AF_UNIX: do {
			struct sockaddr_un *sun = SUN(sa);
			fprintf (f, "AF_UNIX, addr: '%s'\n", sun->sun_path);
			} while (0); break;
		default:
			fprintf (f, "sa_family: %d unknown", sa->sa_family);
	}
} /* fprintSA () */

void fprintTCP (FILE *f, struct tcphdr *tcph) {
	fprintf (f, "sport: %u, dport: %u\n", htons (tcph->source), htons (tcph->dest));
	fprintf (f, "seq: %u, ack: %u\n", htonl (tcph->seq), htonl (tcph->ack_seq));
	fprintf (f, "doff: %d (%d bytes), flags [%c%c%c%c%c%c%c%c]\n", tcph->doff,
		tcph->doff * 4, '.', '.',
		tcph->urg ? 'U' : '.', tcph->ack ? 'A' : '.', tcph->psh ? 'P' : '.',
		tcph->rst ? 'R' : '.', tcph->syn ? 'S' : '.', tcph->fin ? 'F' : '.');
	fprintf (f, "window: %u, checksum: %04x\n", htons (tcph->window), htons (tcph->check));
	// todo - iterate through any options and print out
} /* fprintTCP () */

#define NSCD_SOCK "/run/nscd/socket"
#define NSCD_VERS 2
/* found by counting request_type enum from nscd-client.h in glibc sources */ 
#define NSCD_REQTYPE_INVALIDATE 10

/* sometimes we need to invalidate an NSCD table without fork()ing */
int nscd_invalidate (char *table) {
	struct {
		/* from glibc-2.31/nscd/nscd-client.h */
		struct {
			int32_t version;  /* Version number of the daemon interface.  */
			uint32_t type;    /* Service requested.  */
			int32_t key_len;  /* Key length.  */
		} hdr;
		char table[20];
	} req;
	req.hdr.version = NSCD_VERS;
	req.hdr.type = NSCD_REQTYPE_INVALIDATE;
	req.hdr.key_len = strlen (table) + 1;
	if (req.hdr.key_len > sizeof (req.table)) {
		return -1;
	}
	strcpy (req.table, table);

	int32_t resp = 0;
	int sock = udsConnect (NSCD_SOCK, 250);
	if (sock < 0) return -1;
	do {
		size_t reqlen = sizeof (req.hdr) + req.hdr.key_len;
		ssize_t nbytes = writeall (sock, (char *)&req, reqlen);
		if (nbytes != reqlen) {
			BERR ("send");
			resp = -1;
			break;
		}

		nbytes = readsome (sock, (char *)&resp, sizeof (resp));
		if (nbytes != 0 && nbytes != sizeof (resp)) {
			BERR ("invalid ACK");
			resp = -1;
		}
		BDEBUG ("readsome returned %ld", nbytes);
	} while (0);
	close (sock);
	return resp;
} /* nscd_invalidate () */

#ifdef TESTING
void test_etherscanf () {
	char *buf = "hello";
	uint8_t out[10];
	assert (-1 == etherscanf (NULL, NULL));
	assert (-1 == etherscanf (out, NULL));
	assert (-1 == etherscanf (out, ""));
	assert (-1 == etherscanf (NULL, buf));
	assert (-1 == etherscanf (out, buf));
	buf = "00:50:56:9a:59:9a";
	uint8_t b1[] = {0,0x50,0x56,0x9a,0x59,0x9a};
	assert (0 == etherscanf (out, buf));
	assert (!memcmp (out, b1, 6));
	buf = "00:50:56:9A:59:9a";
	assert (0 == etherscanf (out, buf));
	assert (!memcmp (out, b1, 6));
	buf = "00:50:56:9A:59:9x";
	assert (0 == etherscanf (out, buf));
	uint8_t b2[] = {0,0x50,0x56,0x9a,0x59,9};
	assert (!memcmp (out, b2, 6));
	buf = "00:50:56:9x:59:9a";
	assert (-1 == etherscanf (out, buf));
} /* test_etherscanf () */
#endif

int etherscanf (uint8_t *mac, char *ether) {
	if (!ether || !*ether || !mac) return -1;
	return sscanf (ether, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &mac[0], &mac[1],
		&mac[2], &mac[3], &mac[4], &mac[5]) == 6 ? 0 : -1;
} /* etherscanf () */

#ifdef TESTING
void test_mac2Str () {
	char out[20];
	uint8_t buf[] = {
		0, 1, 0xfc, 4, 2, 1, 128
	};
	assert (NULL == mac2Str (NULL, buf));
	assert (NULL == mac2Str (out, NULL));
	assert (!strcmp ("00:01:fc:04:02:01", mac2Str (out, buf)));
	assert (!strcmp ("01:fc:04:02:01:80", mac2Str (out, &buf[1])));
} /* test_mac2Str () */
#endif

char *mac2Str (char *str, void *buf) {
	if (!str || !buf) return NULL;
    unsigned char *b = buf;
    sprintf (str, "%02x:%02x:%02x:%02x:%02x:%02x",
		b[0], b[1], b[2], b[3], b[4], b[5]);
	return str;
} /* mac2Str () */

#ifdef TESTING
void test_ipv4gai () {
	uint32_t s_addr;
	assert (-1 == ipv4gai (NULL, NULL));
	assert (-1 == ipv4gai (&s_addr, NULL));
	assert (-1 == ipv4gai (&s_addr, ""));
	assert (-1 == ipv4gai (NULL, "localhost"));
	assert (0 == ipv4gai (&s_addr, "1.2.3.4"));
	assert (0x4030201 == s_addr);
	assert (0 == ipv4gai (&s_addr, "localhost"));
	assert (htonl (INADDR_LOOPBACK) == s_addr);
	assert (0 == ipv4gai (&s_addr, "dns.google"));
	assert (0x8080808 == s_addr || htonl (0x8080404) == s_addr);
} /* test_ipv4gai () */
#endif

int ipv4gai (uint32_t *s_addr, char *name) {
	if (!name || !s_addr) return -1;
	struct addrinfo hints;
	struct addrinfo *result;

	memset (&hints, 0, sizeof (hints));
	hints.ai_family = AF_INET;
	int rv = getaddrinfo (name, NULL, &hints, &result);
	if (rv) {
		BERR ("getaddrinfo %s", gai_strerror (rv));
		return -1;
	}
	struct sockaddr_in *sin4 = SIN4(result->ai_addr);
	*s_addr = sin4->sin_addr.s_addr;
	freeaddrinfo (result);
	return 0;
} /* ipv4gai () */

/* search /etc/ethers for matching MAC address */
int findethers (uint8_t *mac, uint32_t s_addr) {
	int rv = -1;
	FILE *fp = fopen ("/etc/ethers", "r");
	if (NULL == fp) {
		BERR ("fopen");
		return -1;
	}

	char *line = NULL;
	size_t len = 0;
	ssize_t nread;

	char *macstr, *hoststr;
	char *saveptr;
	while ((nread = getline (&line, &len, fp)) != -1) {
		if (!*line || *line == '#' || *line == '\n') {
			continue;
		}
		macstr = strtok_r (line, " \t", &saveptr);
		hoststr = strtok_r (NULL, " \n", &saveptr);
		uint32_t t_addr;
		if (ipv4gai (&t_addr, hoststr)) continue;
		if (t_addr == s_addr) {
			rv = 0;
			break;
		}
	}
	fclose (fp);

	if (0 == rv) {
		BDEBUG ("found '%s' for '%s'", macstr, hoststr);
		rv = etherscanf (mac, macstr);
	}

	free (line);
	return rv;
} /* findethers () */

/* get name of interface of directly-connected host */
int getifname (char *iface, uint32_t s_addr) {
	int rv = -1;
	struct ifaddrs *ifaddr, *ifa;
	if (getifaddrs (&ifaddr) == -1) {
		BERR ("getifaddrs");
		return -1;
	}
	for (ifa = ifaddr; ifa; ifa = ifa->ifa_next) {
		if (!ifa->ifa_addr) continue;
		if (AF_INET != ifa->ifa_addr->sa_family) continue;
		struct sockaddr_in *addr = SIN4(ifa->ifa_addr);
		uint32_t mask = SIN4(ifa->ifa_netmask)->sin_addr.s_addr;
		if ((s_addr & mask) == (addr->sin_addr.s_addr & mask)) break;
	}
	if (ifa) {
		strncpy (iface, ifa->ifa_name, IFNAMSIZ);
		iface[IFNAMSIZ - 1] = 0;
		BDEBUG ("iface: '%s'", iface);
		rv = 0;
	}

	freeifaddrs (ifaddr);
	return rv;
} /* getifname () */

/* create and send a Wake-on-LAN magic packet to a MAC address on an interface */
int sendwol (uint8_t *mac, char *iface) {
	int rv = -1;
	if (!mac) return -1;
	uint8_t magic_pkt[17 * 6];
	uint8_t *d, *s;
	d = magic_pkt;
	for (int i = 0; i < sizeof (magic_pkt); i++) {
		if (0 == i % 6) s = mac;
		*d++ = (6 > i) ? 0xff : *s++;
	}

	int sock = socket (AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		BERR ("socket");
		return rv;
	}

	do {
		if (iface && setsockopt (sock, SOL_SOCKET, SO_BINDTODEVICE,
			iface, strlen (iface)) < 0) {
			BERR ("setsockopt SO_BINDTODEVICE '%s'", iface);
			break;
		}
		int opt = 1;
		if (setsockopt (sock, SOL_SOCKET, SO_BROADCAST, &opt,
			sizeof (opt)) < 0) {
			BERR ("setsockopt SO_BROADCAST");
			break;
		}
		struct sockaddr_in sockaddr;
		sockaddr.sin_family = AF_INET;
		sockaddr.sin_port = htons (7);
		sockaddr.sin_addr.s_addr = INADDR_BROADCAST;
		if (sendto (sock, magic_pkt, sizeof (magic_pkt), 0, SIN(&sockaddr),
			sizeof (sockaddr)) <= 0) {
			BERR ("sendto");
			break;
		}
		rv = 0;
	} while (0);

	close (sock);
	return rv;
} /* sendwol () */

/* send a Wake-on-LAN magic packet to host */
int wol (char *host) {
	/* get IPv4 address of host */
	uint32_t s_addr;
	if (ipv4gai (&s_addr, host) != 0) {
		BERR ("ipv4gai");
		return -1;
	}

	/* discover MAC address */
	uint8_t mac[6];
	if (findethers (mac, s_addr) != 0) {
		BERR ("findethers");
		return -1;
	}

	/* get directly-connected network interface */
	char iface[IFNAMSIZ];
	if (getifname (iface, s_addr) != 0) {
		BERR ("getifname");
		return -1;
	}

	/* create and send magic packet */
	return sendwol (mac, iface);
} /* wol () */

#ifdef TESTING
void test_dns_reverse () {
	char *tmp;
	char buf[100];
	struct {
		char *in;
		char *out;
	} *testv, tests[] = {
		{"...", ""},
		{"csitlabs", "csitlabs"},
		{"wikipedia.org", "org.wikipedia"},
		{"liquid.com.", "com.liquid"},
		{"comp.anu.edu.au..", "au.edu.anu.comp"},
		{NULL, NULL}
	};
	assert (NULL == dns_reverse (buf, sizeof (buf), NULL));
	assert (NULL == dns_reverse_alloc (NULL));
	tmp = dns_reverse_alloc ("");
	assert (0 == strlen (tmp));
	free (tmp);
	tmp = dns_reverse_alloc (".");
	assert (0 == strlen (tmp));
	free (tmp);
	for (testv = tests; testv->in; testv++) {
		assert (!strcmp (testv->out, dns_reverse (buf, sizeof (buf), testv->in)));
	}
	testv = &tests[2];
	tmp = dns_reverse_alloc (testv->in);
	assert (!strcmp (testv->out, tmp));
	free (tmp);
}
#endif

/* reverse components of a DNS name */
char *dns_reverse (char *buf, size_t size, char *in) {
	if (!in) return NULL;
	size_t len = strlen (in);
	if (size < len) return NULL;
	char *p = buf;
	while (len > 0) {
		if (in[len - 1] == '.') {
			len--;
			continue;
		}
		size_t start = len - 1;
		size_t num;
		for (num = 0; start && in[start] != '.'; num++) start--;
		if (in[start] =='.') start++;
		if (p != buf) *p++ = '.';
		memcpy (p, &in[start], num + !start);
		p += num + !start;
		len -= (num + 1);
	}
	*p = 0;
	return (buf);
} /* dns_reverse () */

char *dns_reverse_alloc (char *in) {
	if (!in) return NULL;
	size_t len = strlen (in) + 1;
	char *buf = malloc (len);
	return dns_reverse (buf, len, in);
} /* dns_reverse_alloc ()*/
