/*
*/

#ifndef __btar_h__
#define __btar_h__

#include "util.h"

struct file_node *tar_read_obj (int fd);

#endif /* __btar_h__ */
