/*
 * net.h - header for some network functions
 *
 * by Robert Edwards, April 2006
 */

#ifndef _NETLIBH
#define _NETLIBH

#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/tcp.h>
#include <net/if.h>

#include "util.h"

#define SIN(x) ((struct sockaddr *) (x))
#define SIN4(x) ((struct sockaddr_in *) (x))
#define SIN6(x) ((struct sockaddr_in6 *) (x))
#define SUN(x) ((struct sockaddr_un *) (x))
#define VALID_IPV4(hdr,len) ((hdr->version == 4) && (hdr->ihl >= 5) && ((hdr->ihl * 4) <= len) && (ntohs (hdr->tot_len) == len))

struct ipv4List {
	struct ipv4List *next;
	uint32_t addr;
	uint32_t mask;
}; /* struct ipv4List */

struct pseudo_hdr {
	uint32_t src;
	uint32_t dst;
	uint8_t zero;
	uint8_t protocol;
	uint16_t tcp_len;
}; /* struct pseudo_hdr */

#define BDEBUGSA(sa) do { \
	if ((LOG_DEBUG <= DEBUG_LEVEL) && (LOG_DEBUG <= dbglvl)) { \
		fprintf (stderr, "DEBUG: %s:%d:%s(): ", __FILE__, __LINE__, __func__); \
		fprintSA (stderr, sa); \
	} \
} while (0)

#define BDEBUGTCP(tcph) do { \
	if ((LOG_DEBUG <= DEBUG_LEVEL) && (LOG_DEBUG <= dbglvl)) { \
		fprintf (stderr, "DEBUG: %s:%d:%s(): ", __FILE__, __LINE__, __func__); \
		fprintTCP (stderr, tcph); \
	} \
} while (0)

uint32_t get_net_num (void **p, int len);
int readsome (int fd, char *buf, int len);
int writeall (int fd, char *buf, int len);
int readwrite (int in, int out);
size_t transfer (int in, int out);
int write_reconnect (int *fd, char *buf, int len, int ms);
int inSockAddr (struct sockaddr *sa, int port, void *addr);
int inSockHost (struct sockaddr *sa, int port, char *hostname);
int tcpConnectSA (struct sockaddr *sa);
int tcpConnect (char *remoteHost, int port);
int tcp4Listen (int port);
int tcp4Connect (char *host, char *port, int ms);
int udpBind (int port);
int udsConnect (const char *path, int ms);
int udsListen (const char *path, mode_t mode);
uint16_t checksum (void *buf, int len);
int ipv4AddrToDDN (char *str, void *buf);
int dnsLkup (char *dns, char *ddn);
int ddn2bin (char **ip, uint8_t *buf, int size);
struct ipv4List *ipv4ListAdd (struct ipv4List **list, char *ip);
int ipv4Allow (uint32_t addr, int order, struct ipv4List *allow, struct ipv4List *deny);
int getLocalAddr (struct ifaddrs *ifaddr, char *hostname, int len);
char *remHostName (uint32_t IP, char *name, int len);
int strFdOpen (char *str, int ms);
int tcp4Probe (uint32_t addr, int port, int ms);
void *tcp_syn_pkt_new (char *srcIP, int port);
int tcp_syn_tickle (int fd, void *buf, in_addr_t dest, int port, int ms);
in_addr_t ipv4Netmask (in_addr_t ipv4);
void fprintSA (FILE *f, struct sockaddr *sa);
void fprintTCP (FILE *f, struct tcphdr *tcph);
int nscd_invalidate (char *table);
int etherscanf (uint8_t *mac, char *ether);
char *mac2Str (char *str, void *buf);
int ipv4gai (uint32_t *s_addr, char *name);
int findethers (uint8_t *mac, uint32_t s_addr);
int getifname (char *iface, uint32_t s_addr);
int sendwol (uint8_t *mac, char *iface);
int wol (char *host);
char *dns_reverse (char *buf, size_t size, char *in);
char *dns_reverse_alloc (char *in);

#ifdef TESTING
void test_get_net_num ();
void test_checksum ();
void test_etherscanf ();
void test_mac2Str ();
void test_ipv4gai ();
void test_dns_reverse ();
#endif

#endif /* _NETLIBH */
