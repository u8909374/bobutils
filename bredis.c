/*
 * bredis.c - some redis support functions
 *
 * by Robert (bob) Edwards, Aug 2023
 *
 * GPLv2 etc.
 */

#include "bredis.h"

redisContext *openRedis (char *hostport) {
	redisContext *rc;
	char host[256];
	int port = 6379;
	if (!hostport) {
		strcpy (host, "127.0.0.1");
	} else {
		strncpy (host, hostport, sizeof (host) - 1);
		host[sizeof (host) - 1] = 0;
		char *saveptr;
		strtok_r (host, ":", &saveptr);
		if (saveptr && *saveptr) {
			port = atoi (saveptr);
		}
	}
	struct timeval timeout = {1, 500000};

	rc = redisConnectWithTimeout (host, port, timeout);
	if (!rc) {
		BERR ("malloc\n");
		return NULL;
	}
	if (rc->err) {
		BERR ("redisConnect: %s\n", rc->errstr);
		return NULL;
	}
	return rc;
} /* openRedis () */

int redis_ismember (redisContext *redis, char *set, char *val) {
	int rv = 0;
	redisReply *reply;
	reply = redisCommand (redis, "SISMEMBER %s %s", set, val);
	if (!reply) {
		BERR ("redisCommand");
		return -1;
	}
	if (REDIS_REPLY_INTEGER == reply->type) {
		rv = (1 == reply->integer);
	}
	freeReplyObject (reply);
	return rv;
} /* redis_ismember () */
