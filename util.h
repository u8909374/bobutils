/*
 * util.h - header for utility routines
 *
 * by Robert Edwards, April 2006
 */

#ifndef __UTIL_H__
#define __UTIL_H__

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <wait.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <pwd.h>
#include <assert.h>
#include <stdint.h>
#include <sys/mman.h>

#ifndef DEBUG_LEVEL
#ifdef DEBUG
#define DEBUG_LEVEL LOG_DEBUG
#else
#define DEBUG_LEVEL LOG_ERR
#endif
#endif

#ifndef _LOG_LEVEL
#ifdef _LOG
#define _LOG_LEVEL LOG_DEBUG
#else
#define _LOG_LEVEL -1
#endif
#endif

extern uint8_t dbglvl;
extern uint8_t _loglvl;
extern char **_dbgNames;
extern uint8_t bu_issd;

/*
 * adapted from:
 * http://stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing
 * compiler will optimise away if lvl > DEBUG_LEVEL and lvl > _LOG_LEVEL, and
 * runtime messages suppressed if lvl > dbglvl, or
 * syslog messages suppressed if lvl > _loglvl
 * LOG_EMERG normally won't be suppressed and will result in program termination
 */
#define BLOG_RAW(lvl, fmt, ...) do { \
	if (lvl <= DEBUG_LEVEL && lvl <= dbglvl) { \
		fprintf (stderr, "%s: " fmt "\n", _dbgNames[lvl], ##__VA_ARGS__); \
	} \
	if (lvl <= _LOG_LEVEL && lvl <= _loglvl) { \
		syslog (lvl, "%s: " fmt "\n", _dbgNames[lvl], ##__VA_ARGS__); \
	} \
} while (0)

#define BLOG_MSG(lvl, fmt, ...) do { \
	if ((lvl <= DEBUG_LEVEL) || (lvl <= _LOG_LEVEL)) { \
		char *errstr = ""; \
		if (lvl <= LOG_ERR && errno > 0) errstr = strerror (errno); \
		if (lvl <= DEBUG_LEVEL && lvl <= dbglvl) { \
			fprintf (stderr, "%s: %s:%d: " fmt " %s\n", _dbgNames[lvl], \
				__FILE__, __LINE__, ##__VA_ARGS__, errstr); \
		} \
		if (lvl <= _LOG_LEVEL && lvl <= _loglvl) { \
			syslog (lvl, "%s: %s:%d: " fmt " %s\n", _dbgNames[lvl], \
				__FILE__, __LINE__, ##__VA_ARGS__, errstr); \
		} \
	} \
	if (lvl == LOG_EMERG) exit (EXIT_FAILURE); \
} while (0)

#ifndef debug_msg
#define debug_msg(lvl, ...) BLOG_MSG(lvl, "", ##__VA_ARGS__)
#endif

#define BEMERG(...)  BLOG_MSG(LOG_EMERG, ##__VA_ARGS__)
#define BALERT(...)  BLOG_MSG(LOG_ALERT, ##__VA_ARGS__)
#define BCRIT(...)   BLOG_MSG(LOG_CRIT,  ##__VA_ARGS__)
#define BERR(...)    BLOG_MSG(LOG_ERR,   ##__VA_ARGS__)
#define BWARN(...)   BLOG_MSG(LOG_WARNING, ##__VA_ARGS__)
#define BNOTICE(...) BLOG_MSG(LOG_NOTICE,##__VA_ARGS__)
#define BINFO(...)   BLOG_MSG(LOG_INFO,  ##__VA_ARGS__)
#define BDEBUG(...)  BLOG_MSG(LOG_DEBUG, ##__VA_ARGS__)
#define BDEBP1(...)  BLOG_MSG(LOG_DEBUG+1, ##__VA_ARGS__)
#define BDEBP2(...)  BLOG_MSG(LOG_DEBUG+2, ##__VA_ARGS__)

#define BERATELIM(suppress, ...) do { \
	if (!suppress) { \
		BLOG_MSG(LOG_ERR, ##__VA_ARGS__); \
	} else { \
		BLOG_MSG(LOG_DEBUG, ##__VA_ARGS__); \
	} \
} while (0)

#ifndef debug_hexdump
#define debug_hexdump(lvl, str, buf, len) do { \
	if ((lvl <= DEBUG_LEVEL) && (lvl <= dbglvl)) { \
		fprintf (stderr, "%s:%d:%s(): %s (%d bytes)\n", __FILE__, __LINE__, \
			__func__, str, len); \
		fHexDump (stderr, buf, len); \
	} \
} while (0)
#endif

/* following defines from http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html */
#define SetBit(A,k)     ( A[(k/32)] |= (1 << (k%32)) )
#define ClearBit(A,k)   ( A[(k/32)] &= ~(1 << (k%32)) )
#define TestBit(A,k)    ( A[(k/32)] & (1 << (k%32)) )

#define SER_HANDSHAKE_NONE 0
#define SER_HANDSHAKE_HW 1
#define SER_HANDSHAKE_XON 2

#define PRESERVE_TIMES 0x01

#define STRINGIFY(x) _STRINGIFY(x)
#define _STRINGIFY(x) #x

struct file_node {
	struct file_node *next;
	char name[256];		/* long enough for trad. tar file */
	mode_t mode;
	uid_t uid;
	gid_t gid;
	time_t mtime;
	size_t size;
	void *data;
};

struct tree_node {
	void *key;
	uintptr_t left_node;
	struct tree_node *right_node;
};

struct tree_node_list {
	uint32_t pos;
	uint32_t max;
	void **list;
};

int daemonize (void);
pid_t fork_exec (const char *cmd, char *const *args, char *const envp[]);
void sigchld_handler (int s);
int setup_sigchld ();
int is_foreground (void);
void strAppend (char *buf, char *join, char *new);
char *myStrCat (char *dest, int sz, const char *src);
int const_strcmp (const void *l, const void *r);
char *str2lower (char *str);
int hex2num (const char in);
int hex2num2 (char *hex);
char num2hex (const int in);
int strhex2bin (unsigned char *out, const char *in, int len);
void hex_encode (char *dst, const unsigned char *src, size_t srcSize);
char *str2Cs (char *buf, int len, const char *in);
int arrCount (void *arr);
int strArrInd (const char *needle, char **haystack);
void strArrFree (char **arr);
int serialSetup (char *dev, int baud, int raw, int hshk);
int set_nonblock (int fd);
int read_timeout (int sock, void *buf, size_t count, int timeout);
char *getsNoEcho (int fd, char *str, int size);
char *prependDir (char *name, char *dir);
int setPathMode (char *path, mode_t mode);
int ndirents (char *dirname);
int num_threads ();
long clock_ms ();
long interval_ms (struct timespec *t1, struct timespec *t2);
long elapsed_ms (struct timespec *ref);
void sleep_ms (long ms);
int ms2human (long ms, char *buf, size_t len);
int read_timeout (int sock, void *buf, size_t count, int timeout);
int bccCalc (char *buf, int len);
int checkPIDFile (char *name);
int writePIDFile (char *fmt, ...);
int word_count (const char *t);
int parse_args (char *argv[], int numv, char *args);
char *find_arg (int argc, char *argv[], char *t);
char *strncpychr (char *dest, const char *src, size_t n, char delim);
void fHexDump (FILE *f, const void *buf, int len);
void fprintFeatureTest (FILE *f);
void tree_walk (const struct tree_node *this, void (*action) (void *, void *),
	void *closure);
void tree_dump (const struct tree_node *this, int level, FILE *fp, int is_str);
void tree_deleter (struct tree_node *this);
size_t get_tree_nodes (void ***nodes, const void *tree, int num);
int get_quoted_strings (char **out, char *in, int num);
size_t strStrLen (char *sstr);
char *strStrDup (char *sstr);
int strStrNum (char *sstr);
char **strStr2strArr (char *sstr);
char *words2strStr (char *sstr, const char *words);
int is_start_from_systemd ();
int shortmon2int (char *month);
int mmap_copy (int dfd, int sfd, size_t size);
int file_copy (char *dest, char *src);
int symlink_copy (char *dest, char *src);
int recursive_copy (char *dest, char *src, int flags, uid_t uid, gid_t gid, int level);

#ifdef TESTING
void test_word_count ();
void test_parse_args ();
void test_find_arg ();
void test_strncpychr ();
void test_get_quoted_strings ();
void test_strStrNum ();
void test_words2strStr ();
void test_shortmon2int ();
#endif /* TESTING */

#endif /* __UTIL_H__ */
