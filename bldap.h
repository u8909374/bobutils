/*
 * bldap.h
 */

#ifndef __BLDAP_H__
#define __BLDAP_H__

#include <lber.h>
#include <ldap.h>

#include "util.h"

struct ldap_creds {
	char *uri;
	char *basedn;
	char *user;
	char *pass;
};

struct ldap_attr_map {
    char *attr;
    int type;
    char *val;
};

struct ldap_samba {
	char *sambaDomainName;
	char *sambaSID;
	int sambaRIDBase;
};

struct ldap_smbPwd {
	char *sambaLMPassword;
	char *sambaNTPassword;
	char *sambaPwdLastSet;
};

LDAP *ldap_bind (char *uri, char *base, char *user, char *pass);
int ldap_auth (char *uri, char *base, char *user, char *pass);
LDAP *ldap_bind_str (struct ldap_creds *creds);
struct ldap_creds *ldap_fread_creds (char *file);
void ldap_free_creds (struct ldap_creds *creds);
int ldap_get_uid_entry (LDAP *ldap, char *base, char *uid,
	char **attrs, char **vals, int num);
void ldap_fprint_mods (LDAPMod **mods, FILE *fp);
LDAPMod **ldap_map_attrs (struct ldap_attr_map *map,
	char **attrs_in, struct ldap_creds *cred, int op);
void ldap_update_pass (LDAP *ldap, char *dn, char *pass);
struct ldap_samba *ldap_get_sambaSID (char *cred_file);
LDAPMod **ldap_mods_add_SMBPW (LDAPMod **mods, char *uid,
	struct ldap_samba *samba, struct ldap_smbPwd *pwd, int op);
void ldap_dumpAttr_strStr (char *str, FILE *fp);
void ldap_debug_mods (int lvl, LDAPMod **mods);
#define BLDAP_LOG_MOD(lvl, dn, mods) do { \
	BLOG_MSG(lvl, "ldap mods for '%s'", dn); \
	ldap_debug_mods (lvl, mods); \
} while (0)

#endif /* __BLDAP_H__ */
