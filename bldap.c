/*
 * bldap.c
 *
 * useful links:
 *  http://www-archive.mozilla.org/directory/csdk-docs/search.htm#c6-overview-searching-with-ldap-api-functions
 *
 * by Robert (bob) Edwards, July 2023
 *
 * GPLv2 etc.
 */

#include "bldap.h"

void ldap_dumpAttr_strStr (char *str, FILE *fp) {
	int num = 0;
	while (*str) {
		fprintf (fp, num ? "%s'%s'" : "%s%s: ", num > 1 ? ", " : "", str);
		num++;
		str += strlen (str) + 1;
	}
	fprintf (fp, "\n");
} /* ldap_dumpAttr_strStr () */

LDAP *ldap_bind (char *uri, char *base, char *user, char *pass) {
	LDAP *ldap;
	int rv;

	if (!user || !pass || !user[0] || !pass[0] || !uri || !uri[0] || !base) {
		BDEBUG ("missing arg(s)");
		return NULL;
	}

	rv = ldap_initialize (&ldap, uri);
	if (LDAP_SUCCESS != rv) {
		BDEBUG ("ldap_initialize with %s", uri);
		return NULL;
	}

	do {
		int ldap_version = 3;
		char *binddn = user;
		struct berval cred;
		char who[1024];
		who[0] = 0;

	    rv = ldap_set_option (ldap, LDAP_OPT_PROTOCOL_VERSION, &ldap_version);
		if (LDAP_SUCCESS != rv) {
			BDEBUG ("ldap_set_option failed: %s", ldap_err2string (rv));
			break;
		}

		if (!strchr (user, '=')) {
			snprintf (who, sizeof(who), "uid=%s,ou=People,%s", user, base);
			binddn = who;
		}
		BDEBUG ("binddn is '%s'", who);
		cred.bv_val = pass;
		cred.bv_len = strlen (pass);
		rv = ldap_sasl_bind_s (ldap, binddn, LDAP_SASL_SIMPLE, &cred, NULL, NULL, NULL);
		if (LDAP_SUCCESS != rv) {
			BDEBUG ("bind '%s' as '%s' failed: %s", uri, who,
				ldap_err2string (rv));
			break;
		}
		return ldap;
	} while (0);

	ldap_unbind_ext_s (ldap, NULL, NULL);
	return NULL;
} /* ldap_bind () */

/*
 * Authenticate the user "user" with password "pass" against the LDAP server
 * "uri" with LDAP base DN "base".
 * RETURN:
 *     o -1 on authentication or other failure
 *     o 0 on success
 */
int ldap_auth (char *uri, char *base, char *user, char *pass) {
	LDAP *ldap;
	int rv = -1;

	if ((ldap = ldap_bind (uri, base, user, pass)) != NULL) {
		ldap_unbind_ext_s (ldap, NULL, NULL);
		rv = 0;
	}
	return rv;
} /* ldap_auth () */

LDAP *ldap_bind_str (struct ldap_creds *creds) {
	if (!creds) return NULL;
	BDEBUG ("about to bind to '%s,%s' as '%s'", creds->uri, creds->basedn, creds->user);
	return ldap_bind (creds->uri, creds->basedn, creds->user, creds->pass);
} /* ldap_bind_str () */

struct ldap_creds *ldap_fread_creds (char *file) {
	struct ldap_creds *creds;
	char *line = NULL;
    size_t len = 0;
    ssize_t nread;

	FILE *fp = fopen (file, "r");
	if (!fp) {
		BDEBUG ("fopen %s", file);
		return NULL;
	}

	creds = (struct ldap_creds *) calloc (1, sizeof (struct ldap_creds));
	if (!creds) {
		BCRIT ("calloc\n");
		fclose (fp);
		return NULL;
	}

	while ((nread = getline (&line, &len, fp)) != -1) {
		char *tok, *saveptr, *val;
		if (*line == ';' || *line == '#' || *line == '\n') {
			continue;
		}
		tok = str2lower (strtok_r (line, "=: \t", &saveptr));
		if (tok) {
			char *sepstr = "=:; \t\n";
			if ((val = strchr (saveptr, '"'))) {
				saveptr = val;
				sepstr = "\"\n";
			}
			val = strtok_r (NULL, sepstr, &saveptr);
			if (val) {
				if (!strcmp (tok, "ldap_uri")) {
					creds->uri = strdup (val);
				} else if (!strcmp (tok, "ldap_basedn")) {
					creds->basedn = strdup (val);
				} else if (!strcmp (tok, "ldap_user")) {
					creds->user = strdup (val);
				} else if (!strcmp (tok, "ldap_pass")) {
					creds->pass = strdup (val);
				}
			}
		}
	}
	free (line);
	fclose (fp);
	return creds;
} /* fread_creds () */

void ldap_free_creds (struct ldap_creds *creds) {
	if (!creds) return;
	if (creds->pass) {
		memset (creds->pass, 0, strlen (creds->pass));
		free (creds->pass);
	}
	if (creds->user) free (creds->user);
	if (creds->basedn) free (creds->basedn);
	if (creds->uri) free (creds->uri);
	free (creds);
} /* free_creds () */

/* copy array of BER values to a strStr */
char *berval2strStr (struct berval **bvpp, char *attr) {
	char *rv;
	int len = strlen (attr) + 1;
	for (struct berval **vp = bvpp; *vp; vp++) {
		len += (*vp)->bv_len + 1;
		BDEBUG ("    val '%s'", (*vp)->bv_val);
	}
	char *sstr = calloc (1, len + 1);
	if (!sstr) {
		BCRIT ("calloc");
		return NULL;
	}
	rv = sstr;
	strcpy (sstr, attr);
	sstr += strlen (attr) + 1;
	for (struct berval **vp = bvpp; *vp; vp++) {
		strcpy (sstr, (*vp)->bv_val);
		sstr += (*vp)->bv_len + 1;
	}
	*sstr = 0;	/* calloc _should_ have already done this ...*/
	return rv;
} /* berval2strStr () */

int ldap_get_uid_entry (
	LDAP *ldap,
	char *base,
	char *uid,
	char **attrs,
	char **vals,
	int num
) {
	int rv;
	LDAPMessage *res;
	char filter[1024];
	char basedn[1024];

	if (!ldap || !base || !uid || !vals || !num) {
		BDEBUG ("missing args");
		return -1;
	}
	snprintf (basedn, sizeof (basedn), "ou=People,%s", base);
	snprintf (filter, sizeof (filter), "(uid=%s)", uid);
	BDEBUG ("basedn: '%s'", basedn);
	BDEBUG ("filter: '%s'", filter);

	rv = ldap_search_ext_s (ldap, basedn, LDAP_SCOPE_SUBTREE, filter, attrs,
		0, NULL, NULL, NULL, LDAP_NO_LIMIT, &res);
	if (rv!= LDAP_SUCCESS) {
		BDEBUG ("ldap_search_ext: %s", ldap_err2string (rv));
		return -1;
	}

	if (1 != ldap_count_entries (ldap, res)) {
		BDEBUG ("no entries found");
		return -1;
	}

	rv = 0;
	LDAPMessage *msg = ldap_first_message (ldap, res);
	for (; msg; msg = ldap_next_message (ldap, msg)) {
		if (LDAP_RES_SEARCH_ENTRY != ldap_msgtype (msg)) {
			continue;
		}
		BerElement *ber;
		char *attr = ldap_first_attribute (ldap, msg, &ber);
		for (; num && attr; attr = ldap_next_attribute (ldap, msg, ber)) {
			struct berval **bvpp = ldap_get_values_len (ldap, msg, attr);
			if (!bvpp) {
				BDEBUG ("attr %s has no values\n", attr);
				continue;
			}
			*vals++ = berval2strStr (bvpp, attr);
			num--;
			rv++;
			ldap_value_free_len (bvpp);
		}
		*vals = NULL;
		ldap_memfree (attr);
		if (ber) ber_free (ber, 0);
	}
	ldap_msgfree (res);
	return rv;
} /* ldap_get_uid_entry () */

void ldap_fprint_mods (LDAPMod **mods, FILE *fp) {
	char *op_name[3] = {"add ", "del ", "repl"};
	if (!mods || !*mods) {
		BDEBUG ("mods is NULL or empty");
	}
	while (*mods) {
		LDAPMod *mod = *mods;
		if (mod->mod_op & LDAP_MOD_BVALUES) {
			fprintf (fp, "ber vals\n");
		} else {
			for (char **s = mod->mod_vals.modv_strvals; *s; s++) {
				fprintf (fp, "%s '%s': %s\n", op_name[mod->mod_op & 3],
					mod->mod_type, *s);
			}
		}
		mods++;
	}
} /* ldap_fprint_mods () */

/* copy static words to strArr */
char **words2strArr (const char *words) {
	char buf[1024];
	return strStr2strArr (words2strStr (buf, words));
} /* words2strArr () */

/* find matching attr in attrs and return strArr of vals */
char **matchedAttr2strArr (const char *match, char **attrs) {
	int ind = strArrInd (match, attrs);
	if (ind < 0) {
		BNOTICE ("no attr '%s' found", match);
		return NULL;
	}
	BDEBUG ("found mapped attr: '%s' at %d", match, ind);
	char *c = attrs[ind];
	return strStr2strArr (c + strlen (c) + 1);
} /* matchedAttr2strArr () */

/* format a string according to format, return strArr */
char **format_strArr (const char *format, const char *str) {
	char buf[1024];
	int len = snprintf (buf, sizeof (buf), format, str);
	buf[len + 1] = 0;	/* turn it into a sstr */
	return strStr2strArr (buf);
} /* format_strArr () */

/* allocate and return a new LDAPMod */
LDAPMod *new_str_mod (int op, const char *type, char **vals) {
	LDAPMod *mod = (LDAPMod *) calloc (1, sizeof (LDAPMod));
	char *t = strdup (type);
	if (!mod || !t) {
		free (mod);
		free (t);
		BCRIT ("calloc");
		return NULL;
	}
	mod->mod_op = op;
	mod->mod_type = t;
	mod->mod_vals.modv_strvals = vals;
	BDEBUG ("new_str_mod: added %s", t);
	return mod;
} /* new_str_mod () */

/*
 * take a null-terminated array of ldap_attr_map objs and returns a
 * null-terminated array of LDAPMod * objs, suitable for use with
 * ldap_add_ext () and ldap_modify_ext () (and ldap_mods_free (3))
 *
 * attrs_in is an array of string-of-strings with attributes from a
 * an LDAP query (eg. from ldap_get_uid_entry () above). cred is the
 * users credentials, in particular, username and password. op is
 * the operation to perform for all attributes, typically LDAP_MOD_ADD
 * or LDAP_MOD_REPLACE.
 */
LDAPMod **ldap_map_attrs (
	struct ldap_attr_map *map,
	char **attrs_in,
	struct ldap_creds *cred,
	int op
) {
	int num = 5;	/* leave some room for samba attrs */

	for (struct ldap_attr_map *al = map; al->attr; al++) num++;
	LDAPMod **rv = (LDAPMod **) calloc (num, sizeof (LDAPMod *));
	if (!rv) {
		BCRIT ("calloc");
		return NULL;
	}
	LDAPMod **mods = rv;
	for (struct ldap_attr_map *al = map; al->attr; al++) {
		char **vals = NULL;
		BDEBUG ("doing attr: '%s', type: %d", al->attr, al->type);
		switch (al->type) {
			case 1:
				vals = words2strArr (al->val);
				break;
			case 2:
				vals = matchedAttr2strArr (al->val, attrs_in);
				break;
			case 3:
				vals = format_strArr (al->val, cred->user);
				break;
		}
		if (!vals) {
			BDEBUG ("vals is NULL");
			break;
		}
		*mods = new_str_mod (op, al->attr, vals);
		mods++;
	}
	*mods = NULL;
	return rv;
} /* ldap_map_attrs () */

void ldap_update_pass (LDAP *ldap, char *dn, char *pass) {
	struct berval bv = {0, NULL};
	struct berval newpw;
	BerElement *ber = NULL;
	int rc;

	if (!ldap || !dn || !pass) {
		BDEBUG ("missing args");
		return;
	}

	newpw.bv_val = strdup (pass);
	newpw.bv_len = strlen (pass);

	if (!(ber = ber_alloc_t (LBER_USE_DER))) {
		BDEBUG ("ber_alloc_t");
		return;
	}

	do {
		char *retoid = NULL;
		struct berval *retdata = NULL;

		ber_printf (ber, "{" /*}*/ );
		ber_printf (ber, "ts", LDAP_TAG_EXOP_MODIFY_PASSWD_ID, dn);
		ber_printf (ber, "tO", LDAP_TAG_EXOP_MODIFY_PASSWD_NEW, &newpw);
		free (newpw.bv_val);
		ber_printf (ber, /*{*/ "N}");

		rc = ber_flatten2 (ber, &bv, 0);
		if (rc < 0) {
			BDEBUG ("ber_flatten2");
			break;
		}

		rc = ldap_extended_operation_s (ldap, LDAP_EXOP_MODIFY_PASSWD,
			&bv, NULL, NULL, &retoid, &retdata);
		if (retdata) ber_bvfree (retdata);
		if (retoid) ldap_memfree (retoid);
	} while (0);
	if (LDAP_SUCCESS != rc) {
		BDEBUG ("ldap_ext_op_s: %s", ldap_err2string (rc));
	} else {
		BDEBUG ("ldap_ext_op_s: good");
	}
	ber_free (ber, 1);
} /* ldap_update_pass () */

struct ldap_samba *ldap_get_sambaSID (char *cred_file) {
	struct ldap_samba *samba;
	int rv = 0;
	struct ldap_creds *bind_creds;
	LDAP *ldap = NULL;
	LDAPMessage *res = NULL;

	if (!(samba = calloc (1, sizeof (struct ldap_samba)))) {
		BERR ("calloc");
		return NULL;
	}
	if (!(bind_creds = ldap_fread_creds (cred_file))) {
		BERR ("failed to read creds");
		return NULL;
	}

	do {
		ldap = ldap_bind_str (bind_creds);
		if (!ldap) {
			BERR ("ldap_bind_str");
			break;
		}
		BDEBUG ("ldap_bind_str: OK");

		rv = ldap_search_ext_s (ldap, bind_creds->basedn, LDAP_SCOPE_SUBTREE,
			"sambaDomainName=*", NULL, 0, NULL, NULL, NULL, LDAP_NO_LIMIT, &res);
		if (rv != LDAP_SUCCESS) {
			BDEBUG ("ldap_search_ext: %s", ldap_err2string (rv));
			break;
		}

		if (1 < ldap_count_entries (ldap, res)) {
			BDEBUG ("no entries found");
			break;
		}

		BDEBUG ("dn is '%s'", ldap_get_dn (ldap, res));

		rv = 0;
		LDAPMessage *msg = ldap_first_message (ldap, res);
		for (; msg; msg = ldap_next_message (ldap, msg)) {
			if (LDAP_RES_SEARCH_ENTRY != ldap_msgtype (msg)) {
				continue;
			}
			struct berval **bvpp;
			bvpp = ldap_get_values_len (ldap, msg, "sambaDomainName");
			if (bvpp) {
				samba->sambaDomainName = strdup ((*bvpp)->bv_val);
				ldap_value_free_len (bvpp);
			}
			bvpp = ldap_get_values_len (ldap, msg, "sambaSID");
			if (bvpp) {
				samba->sambaSID = strdup ((*bvpp)->bv_val);
				ldap_value_free_len (bvpp);
			}
			bvpp = ldap_get_values_len (ldap, msg, "sambaAlgorithmicRidBase");
			if (bvpp) {
				samba->sambaRIDBase = atoi ((*bvpp)->bv_val);
				ldap_value_free_len (bvpp);
			}
		}
		BDEBUG ("sambaDomainName: '%s', sambaSID: '%s'", samba->sambaDomainName,
			samba->sambaSID);
	} while (0);
	if (res) ldap_msgfree (res);
	if (ldap) ldap_unbind_ext_s (ldap, NULL, NULL);
	ldap_free_creds (bind_creds);
	return samba;
} /* ldap_get_sambaSID () */

/* add SambaSID and pwds to mods */
LDAPMod **ldap_mods_add_SMBPW (
	LDAPMod **mods,
	char *uid,
	struct ldap_samba *samba,
    struct ldap_smbPwd *pwd,
	int op
) {
	if (!mods || !samba || !samba->sambaSID || !*samba->sambaSID) {
		BDEBUG ("no sambaSID");
		return mods;
	}
	/* get end of mods array */
	while (*mods) mods++;
	char buf[1024];
	int sambaSIDext = 1000 + atoi (uid) * 2;
	snprintf (buf, sizeof (buf), "%s-%d", samba->sambaSID, sambaSIDext);
	*mods++ = new_str_mod (op, "sambaSID", words2strArr (buf));
	*mods++ = new_str_mod (op, "sambaLMPassword", words2strArr (pwd->sambaLMPassword));
	*mods++ = new_str_mod (op, "sambaNTPassword", words2strArr (pwd->sambaNTPassword));
	*mods++ = new_str_mod (op, "sambaPwdLastSet", words2strArr (pwd->sambaPwdLastSet));
	return mods;
} /* ldap_mods_add_SMBPW () */

void ldap_debug_mods (int lvl, LDAPMod **mods) {
	char *op_name[3] = {"add ", "del ", "repl"};
	if (!mods || !*mods) {
		BLOG_RAW(lvl, "mods is NULL or empty");
	}
	while (*mods) {
		LDAPMod *mod = *mods;
		if (mod->mod_op & LDAP_MOD_BVALUES) {
			BLOG_RAW (lvl, "ber vals");
		} else {
			for (char **s = mod->mod_vals.modv_strvals; *s; s++) {
				BLOG_RAW (lvl, "%s '%s': %s", op_name[mod->mod_op & 3],
					mod->mod_type, *s);
			}
		}
		mods++;
	}
} /* ldap_debug_mods () */
