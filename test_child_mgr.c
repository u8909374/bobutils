/*
 * test_child_mgr.c - simple test program for child_mgr.[ch]
 *
 * creates 5 threads, fork-execing two scripts.
 * 4 threads wait for exit status, 5th script ignores it
 *
 * by Robert (bob) Edwards, July 2023
 *
 * GPLv2 etc.
 */

#define DEBUG

#include "child_mgr.h"

#define script_1 "test_child_mgr1.sh"
#define script_2 "test_child_mgr2.sh"

void *childmgr;

void *test_worker (void *arg) {
	int delay = *(int *)arg;

	char *args[4];
	args[0] = script_1;
	args[1] = "Hi!";
	args[2] = NULL;

	while (1) {
		pid_t pid = fork_exec (args[0], args, NULL);
		while (1) {
			int stat = childmgr_getstat (pid);
			if (stat < 0) {
				BDEBUG ("pid %d not finished - %d", pid, delay);
				sleep_ms (2000);
				continue;
			}
			BINFO ("pid %d returned %d", pid, stat);
			break;
		}
		sleep_ms (delay);
		BDEBUG ("lets do it all again...");
	}
} /* test_worker () */

void *test_worker2 (void *arg) {
	int delay = *(int *)arg;

	char *args[4];
	args[0] = script_2;
	args[1] = "Ho!";
	char buf[20];
	snprintf (buf, sizeof (buf), "%d", delay);
	args[2] = buf;
	args[3] = NULL;

	while (1) {
		args[2] = buf;
		fork_exec (args[0], args, NULL);
		sleep_ms (delay);
		BDEBUG ("worker2 - doin' it all again...");
	}
} /* test_worker2 () */

int main (int argc, char *argv[]) {
	pthread_t tid;
	dbglvl = 6;
	int delays[5];
	delays[0] = 1000;
	delays[1] = 5000;
	delays[2] = 4000;
	delays[3] = 6000;
	delays[4] = 2000;
	int count = 0;

	if (childmgr_new (6000) < 0) {
		BEMERG ("childmgr_new");
	}

	for (int i = 0; i < 4; i++) {
		if (pthread_create (&tid, NULL, test_worker, &delays[i])) {
			BEMERG ("pthread_create");
		}
	}
	if (pthread_create (&tid, NULL, test_worker2, &delays[4])) {
		BEMERG ("pthread_create");
	}

	while (1) {
		sleep (10);
		childmgr_dump (stdout);
		printf ("hello - - - - - - - - - - count %d\n", count++);
	}
	return EXIT_SUCCESS;
} /* main () */
