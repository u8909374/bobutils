/*
 * bredis.h - some redis support functions
 *
 * by Robert (bob) Edwards, Aug 2023
 *
 * GPLv2 etc.
 */

#ifndef __BREDIS_H__
#define __BREDIS_H__

#include <hiredis/hiredis.h>

#include "util.h"

redisContext *openRedis (char *hostport);
int redis_ismember (redisContext *redis, char *set, char *val);

#endif /* __BREDIS_H__ */
