

#ifndef __CHILD_MGR_H__
#define __CHILD_MGR_H__

#include <pthread.h>

#include "util.h"

extern void *childmgr;

int childmgr_getstat (pid_t pid);
int childmgr_new (int timeout);
int childmgr_dump ();

#endif /* __CHILD_MGR_H__ */
