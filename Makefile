CFLAGS += -Wall
CFLAGS += -DTESTING
CC := gcc $(CFLAGS)

all: test_util test_netlib test_child_mgr

version.h: 
	./mk_version_h.sh > $@

test_util: test_util.c util.c util.h version.h
	$(CC) -o $@ $< util.c

test_netlib: test_netlib.c netlib.c util.c netlib.h util.h
	$(CC) -o $@ $< netlib.c util.c

test_child_mgr: test_child_mgr.c child_mgr.c child_mgr.h util.c util.h
	$(CC) -o $@ $< child_mgr.c util.c -lpthread

clean:
	rm -f test_util test_netlib version.h
