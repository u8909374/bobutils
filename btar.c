/*
*/

#include <sys/stat.h>
#include <tar.h>
#include "btar.h"

struct tar_header {
	char name[100];
	char mode[8];
	char uid[8];
	char gid[8];
	char size[12];
	char mtime[12];
	char chksum[8];
	char typeflag[1];
	char linkname[100];
	char magic[6];
	char version[2];
	char uname[32];
	char gname[32];
	char devmajor[8];
	char devminor[8];
	char prefix[155];
	char padding[12];
};

struct file_node *tar_read_obj (int fd) {
	struct file_node *new = NULL;
	void *data = NULL;
	int blks_read = 0;
	int expected_blks = 2;	/* assume end of file marker */
	while (expected_blks > 0) {
		expected_blks--;
		BDEBUG ("expected_blks: %d", expected_blks);
		char buf[512];
		int nread = read (fd, buf, sizeof (buf));
		if (nread <= 0) {
			BERR ("read");
			expected_blks = -1;
			break;
		}
		blks_read++;
		if (new) {
			int len = expected_blks ? sizeof (buf) : new->size % sizeof (buf);
			memcpy (data, buf, len);
			data += sizeof (buf);
			continue;
		}
		struct tar_header *hdr = (struct tar_header *)buf;
		if ((blks_read == 1) && !strncmp (hdr->magic, TMAGIC, 5)) {
			mode_t mode = 0;
			switch (hdr->typeflag[0]) {
				case '\0':
				case '0': mode = S_IFREG; break;
				case '2': mode = S_IFLNK; break;
				case '5': mode = S_IFDIR; break;
			}
			if (mode) {
				new = (struct file_node *) calloc (1, sizeof (struct file_node));
				if (!new) {
					BCRIT ("calloc");
					return NULL;
				}
				if (hdr->prefix[0]) {
					snprintf (new->name, sizeof (new->name) - 1, "%s/%s",
						hdr->prefix, hdr->name);
				} else {
					strncpy (new->name, hdr->name, sizeof (hdr->name) - 1);
				}
				new->mode = strtoul (hdr->mode, NULL, 8);
				new->mode |= mode;
				new->uid = strtoul (hdr->uid, NULL, 8);
				new->gid = strtoul (hdr->gid, NULL, 8);
				new->mtime = strtoul (hdr->mtime, NULL, 8);
				new->size = strtoul (hdr->size, NULL, 8);
				BDEBUG ("obj: '%s' size: %ld", new->name, new->size);
				if (new->size) {
					data = malloc (new->size);
					if (!data) {
						BCRIT ("malloc");
						expected_blks = -1;
						break;
					}
					new->data = data;
				} else if (S_IFLNK == (mode & S_IFLNK)) {
					new->data = strdup (hdr->linkname);
					if (!new->data) {
						BCRIT ("strdup");
						expected_blks = -1;
						break;
					}
				}
				expected_blks = ((new->size + 511) / 512);
				continue;
			}
			BNOTICE ("unsupported type '%c'", hdr->typeflag[0]);
			errno = ENOTSUP;
		}
		long int *n = (long int *)buf;
		for (int i = 0; i < (sizeof (buf) / sizeof (long int)); i++) {
			if (*n++) {
				BERR ("not expected 0 tar block at %d, %ld", i, *n);
				expected_blks = -2;
				errno = EPROTO;
				break;
			}
		}
		blks_read = 0;
		errno = 0;
	}
	if (expected_blks < 0) {
		if (new) free (new);
		new = NULL;
	}
	return new;
} /* tar_read_obj () */

