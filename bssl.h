/*
 * bssl.h - header for my SSL wrappers
 *
 * by Robert (bob) Edwards, July 2023
 *
 * GPLv2 etc.
 */

#ifndef __BSSL_H__
#define __BSSL_H__

#define BSSL_SSLv23_METHOD 0
#define BSSL_TLS_METHOD 1

#include <openssl/ssl.h>
#include <openssl/err.h>

#include "util.h"
#include "netlib.h"

SSL_CTX *ssl_init_simple_pem (char *cert);
void print_ssl (SSL *ssl, char *fmt, ...);
int ssl_readline (SSL *ssl, char *buf, int len);
SSL_CTX *ssl_new_client (int method);
SSL *ssl_connect (SSL_CTX *ctx, char *host, char *port, int ms);
SSL *ssl_accept (SSL_CTX *ctx, int fd);
void ssl_fprint_cert (SSL* ssl, FILE *file);
SSL *ssl_close (SSL *ssl);

#endif /* __BSSL_H__ */
