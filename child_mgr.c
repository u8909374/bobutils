/*
 * childmgr.c - routines for a pthreaded program to return exit codes of children
 *
 * uses classic "self-pipe" technique
 *
 * by Robert (bob) Edwards, July 2023
 *
 * GPLv2 etc.
 */

#include <search.h>
#include <pthread.h>
#include <fcntl.h>              /* Obtain O_* constant definitions */
#include <unistd.h>
#include <poll.h>

#define DEBUG
#include "util.h"

struct cmgr_node {
	pid_t pid;
	int wstatus;
	long ms;
};

struct childmgr {
	pthread_t tid;
	pthread_mutex_t mutex;
	int fds[2];
	void *pids;
	long last_ms;
	long timeout;
};

extern void *childmgr;

/*
 * classic "self-pipe" signal handler for SIGCHLD
 */
static void chldmgr_handler (int s) {
	(void)s;
	if (!childmgr) return;
	// write () might overwrite errno...
	int saved_errno = errno;
	struct childmgr *this = (struct childmgr *)childmgr;
	write (this->fds[1], "b", 1);
	errno = saved_errno;
} /* chldmgr_handler () */

static int cmgr_cmp (const void *l, const void *r) {
	return ((struct cmgr_node *)l)->pid - ((struct cmgr_node *)r)->pid;
} /* cmgr_cmp () */

static struct tree_node *clean_pids (struct tree_node *this, long aged_ms) {
	if (!this) {
		BDEBP1 ("cleaning null node");
		return NULL;
	}
	struct tree_node *left, *right, *rv;
	left = (struct tree_node *)(this->left_node & ~((uintptr_t) 0x1));
	right = this->right_node;
	if (left) {
        rv = clean_pids (left, aged_ms);
		if (rv) return rv;
	}
	struct cmgr_node *node = (struct cmgr_node *) this->key;
	if (node->ms < aged_ms) return this;
	if (right) {
		rv = clean_pids (right, aged_ms);
		if (rv) return rv;
	}
	return NULL;
} /* clean_pids () */

static void *childmgr_worker (void *args) {
	struct childmgr *this = (struct childmgr *)childmgr;
	long last_clean = clock_ms ();
	int fd = this->fds[0];
	sigset_t sigmask;
	sigemptyset (&sigmask);
	sigaddset (&sigmask, SIGCHLD);
	pthread_sigmask (SIG_UNBLOCK, &sigmask, NULL);
	struct pollfd pfd;
	pfd.fd = fd;
	pfd.events = POLLIN;

	while (1) {
		struct cmgr_node *new, *old;
		long remain = (this->timeout / 2) - (clock_ms () - last_clean);
		if (remain < 0) remain = 0;

		int rv = poll (&pfd, 1, remain);
		if (rv < 0) {
			BERR ("poll");
			continue;
		}
		if (0 == rv) {
			BDEBP1 ("starting cleanup - last done at %ld", last_clean);
			long aged_ms = clock_ms () - this->timeout;
			int count = 0;
			int ok = 0;
			pid_t pid;
			do {
				ok = 0;
				if (pthread_mutex_lock (&this->mutex)) {
					BERR ("pthread_mutex_lock");
					break;
				}
				struct tree_node *node;
				node = clean_pids (this->pids, aged_ms);
				if (node) {
					old = (struct cmgr_node *)(node->key);
					pid = old->pid;
					tdelete (old, &this->pids, cmgr_cmp);
					free (old);
					ok = 1;
				}
				pthread_mutex_unlock (&this->mutex);
				if (ok) {
					count++;
					BDEBP1 ("cleaned pid %d", pid);
				}
			} while (ok);
			BDEBP1 ("finished cleaning %d pids cleaned", count);
			last_clean = clock_ms ();
			continue;
		}
		if (POLLIN == pfd.revents) {
			char b2[4];
			read (fd, b2, sizeof (b2));
			while (1) {
				int wstatus;
				pid_t pid = waitpid (-1, &wstatus, WNOHANG);
				if (pid <= 0) {
					break;
				}
				BDEBUG ("got node.pid %d, stat %d", pid, wstatus);
				new = malloc (sizeof (struct cmgr_node));
				if (!new) {
					BCRIT ("malloc");
					continue;
				}
				new->pid = pid;
				new->wstatus = wstatus;
				new->ms = clock_ms ();

				if (pthread_mutex_lock (&this->mutex)) {
					BERR ("pthread_mutex_lock");
					continue;
				}
				struct cmgr_node **res = tsearch (new, &this->pids, cmgr_cmp);
				int added = (*res == new);
				pthread_mutex_unlock (&this->mutex);

				if (added) {
					BDEBUG ("pid %d added", pid);
					continue;
				}
				BDEBUG ("pid %d not replaced", pid);
				free (new);
			}
			continue;
		}
		BERR ("poll returned unexpected events %d", pfd.revents);
	}
	return NULL;
} /* childmgr_worker () */

/*
 * get exit status of child process pid, and delete from tree, if found
 */
int childmgr_getstat (pid_t pid) {
	if (!childmgr) {
		BERR ("childmgr not initialised");
		return -1;
	}
	struct childmgr *this = (struct childmgr *)childmgr;
	struct cmgr_node node;
	node.pid = pid;
	pid_t found_pid = 0;
	int stat;

	if (pthread_mutex_lock (&this->mutex)) {
		BERR ("pthread_mutex_lock");
		return -1;
	}
	struct cmgr_node **res;
	res = tfind (&node, &this->pids, cmgr_cmp);
	if (res) {
		struct cmgr_node *found = *res;
		found_pid = found->pid;
		stat = found->wstatus;
		tdelete (found, &this->pids, cmgr_cmp);
		free (found);
	}
	pthread_mutex_unlock (&this->mutex);

	if (found_pid) {
		BINFO ("exit status pid %d: %d", found_pid, stat);
		return stat;
	}
	return -1;
} /* childmgr_getstat () */

/*
 * initialise the childmgr, to an extern void * called childmgr
 */
int childmgr_new (int timeout) {
	if (childmgr) {
		BERR ("childmgr already initialised");
		return -1;
	}
	childmgr = calloc (1, sizeof (struct childmgr));
	if (!childmgr) {
		BEMERG ("calloc");
	}
	struct childmgr *this = (struct childmgr *)childmgr;

	if (pipe (this->fds) < 0) {
		BEMERG ("pipe");
	}

	/* write end (inside signal handler) needs to be non-blocking */
	int fsflags = fcntl (this->fds[1], F_GETFL);
	fcntl (this->fds[1], F_SETFL, fsflags | O_NONBLOCK);
	fcntl (this->fds[0], F_SETFD, fcntl (this->fds[0], F_GETFD) | O_CLOEXEC);
	fcntl (this->fds[1], F_SETFD, fcntl (this->fds[1], F_GETFD) | O_CLOEXEC);

	struct sigaction sa;
	sa.sa_flags = 0;
	sigemptyset (&sa.sa_mask);
	sa.sa_handler = chldmgr_handler;
	if (sigaction (SIGCHLD, &sa, NULL) == -1) {
		BEMERG ("sigaction");
	}

	if (pthread_mutex_init (&this->mutex, NULL)) {
		BEMERG ("pthread_mutex_init");
	}

	this->last_ms = clock_ms ();
	this->timeout = timeout;

	if (pthread_create (&this->tid, NULL, childmgr_worker, NULL)) {
		BEMERG ("pthread_create");
	}
	return 0;
} /* childmgr_new () */

/*
 * iterate through tree of pids and collect them into array pointed to by pos
 */
static struct cmgr_node *collector (
	struct tree_node *node,
	struct cmgr_node *pos
) {
	if (!node || !pos) {
		BDEBUG ("collecting null node");
		return NULL;
	}
	struct tree_node *left, *right;
	left = (struct tree_node *)(node->left_node & ~((uintptr_t) 0x1));
	right = node->right_node;
	if (left) {
		pos = collector (left, pos);
	}
	struct cmgr_node *this = *(struct cmgr_node **) node;
	BDEBP1 ("      Inside collector: pid: %d", this->pid);
	pos->pid = this->pid;
	pos->wstatus = this->wstatus;
	pos->ms = this->ms;
	pos++;
	if (right) {
		pos = collector (right, pos);
	}
	BDEBP1 ("      Pos %p", pos);
	return pos;
} /* collector () */

/*
 * dump current tree of pids to file pointer
 */
void childmgr_dump (FILE *fp) {
	if (!childmgr || !fp) {
		BERR ("args");
		return;
	}
	struct childmgr *this = (struct childmgr *)childmgr;
	struct cmgr_node nodes[100];
	struct cmgr_node *node = nodes;
	long now = clock_ms ();

	if (pthread_mutex_lock (&this->mutex)) {
		BERR ("pthread_mutex_lock");
		return;
	}
	node = collector (this->pids, node);
	pthread_mutex_unlock (&this->mutex);
	int num = node - nodes;
	fprintf (fp, "num collected: %d\n", num);
	for (int i = 0; i < num; i++) {
		fprintf (fp, "    pid: %d, stat %d age %ld\n", nodes[i].pid,
			nodes[i].wstatus, now - nodes[i].ms);
	}
} /* childmgr_dump () */
