#!/bin/bash

GIT_VERSION=`git describe --dirty --always --tags`
DATE=`date`

echo "#ifndef _VERSION_H"
echo "#define _VERSION_H"
echo
echo "#define BUILD_GIT_SHA \"${GIT_VERSION}\""
echo "#define BUILD_DATE \"${DATE}\""
echo
echo "#endif"
