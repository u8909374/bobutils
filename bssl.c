/*
 * bssl.c - my SSL function wrappers
 *
 * by Robert (bob) Edwards, July, 2023
 *
 * GPLv2 etc.
 */

#include "bssl.h"

SSL_CTX *ssl_init_simple_pem (char *cert) {
	SSL_CTX *ssl_ctx = NULL;
	OpenSSL_add_all_algorithms ();
	SSL_load_error_strings ();
	do {
		ssl_ctx = SSL_CTX_new (SSLv23_server_method ());
		if (!ssl_ctx) {
			BERR ("SSL_CTX_new");
			break;
		}
		if (!SSL_CTX_use_certificate_file (ssl_ctx, cert, SSL_FILETYPE_PEM)) {
			BERR ("SSL_CTX_use_certificate_file: %s", cert);
			break;
		}
		if (!SSL_CTX_use_PrivateKey_file (ssl_ctx, cert, SSL_FILETYPE_PEM)) {
			BERR ("SSL_CTX_use_PrivateKey_file");
			break;
		}
		if (!SSL_CTX_check_private_key (ssl_ctx)) {
			BERR ("SSL_CTX_check_private_key");
			break;
		}
		return ssl_ctx;
	} while (0);

	if (ssl_ctx) {
		SSL_CTX_free (ssl_ctx);
	}
	return NULL;
} /* ssl_init_simple_pem () */

void print_ssl (SSL *ssl, char *fmt, ...) {
	va_list args;
	char text[1024];

	va_start (args, fmt);
	vsnprintf (text, sizeof(text), fmt, args);
	va_end (args);
	SSL_write (ssl, text, strlen(text));
} /* print_ssl () */

int ssl_readline (SSL *ssl, char *buf, int len) {
	int pos = 0;
	do {
		if (SSL_read (ssl, buf, 1) <= 0) {
			break;
		}
		if (*buf++ == '\n') {
			break;
		}
	} while (pos++ < len);
	*buf = 0;
	return pos;
} /* ssl_readline () */

SSL_CTX *ssl_new_client (int method) {
	SSL_CTX *ctx;
	const SSL_METHOD *meth = SSLv23_client_method ();

	OpenSSL_add_all_algorithms ();
	SSL_load_error_strings ();
	if (method == BSSL_TLS_METHOD) {
		meth = TLS_client_method();
	}
	ctx = SSL_CTX_new (meth);
	if (!ctx) {
		BERR ("SSL_CTS_new: %s", ERR_error_string (ERR_get_error (), NULL));
	}
	return ctx;
} /* ssl_client_ctx () */

SSL *ssl_connect (SSL_CTX *ctx, char *host, char *port, int ms) {
	SSL *ssl = NULL;
	int fd = -1;

	if (!host || !port || !host[0] || !port[0]) {
		BERR ("invalid args");
		return NULL;
	}

	if (!ctx && !(ctx = ssl_new_client (BSSL_SSLv23_METHOD))) {
		BDEBUG ("ssl_new_client");
		return NULL;
	}

	if ((fd = tcp4Connect (host, port, ms)) < 0) {
		BDEBUG ("tcp_connect");
		return NULL;
	}

	do {
		if (!(ssl = SSL_new (ctx))) {
			BERR ("SSL_new");
			break;
		}

		SSL_set_fd (ssl, fd);

		if (-1 == SSL_connect (ssl)) {
			BDEBUG ("SSL_connect");
			break;
		}
		return ssl;
	} while (0);

	if (ssl) {
		SSL_free (ssl);
	}
	if (fd >= 0) {
		close (fd);
	}
	return NULL;
} /* ssl_connect () */

SSL *ssl_accept (SSL_CTX *ctx, int fd) {
	SSL *ssl = SSL_new (ctx);
	if (!ssl) {
		BERR ("SSL_new");
		return NULL;
	}

	SSL_set_fd (ssl, fd);
	if (SSL_accept (ssl) <= 0) {
		BERR ("SSL_accept");
		SSL_shutdown (ssl);
		SSL_free (ssl);
		return NULL;
	}
	return ssl;
} /* ssl_accept () */

void ssl_fprint_cert (SSL* ssl, FILE *file) {
	X509 *cert;
	char *line;

	cert = SSL_get_peer_certificate (ssl);
	if (cert) {
		fprintf (file, "server certificate(s):\n");
		line = X509_NAME_oneline (X509_get_subject_name (cert), 0, 0);
		fprintf (file, "subject: %s\n", line);
		free (line);
		line = X509_NAME_oneline (X509_get_issuer_name (cert), 0, 0);
		fprintf (file, "issuer: %s\n", line);
		free (line);
		X509_free (cert);
	} else {
		fprintf (file, "no server certificates available\n");
	}
} /* ssl_fprint_cert () */

/* shutdown and close an SSL connection */
SSL *ssl_close (SSL *ssl) {
	if (!ssl) return NULL;
	int sslfd = SSL_get_fd (ssl);
	SSL_shutdown (ssl);
	SSL_free (ssl);
	close (sslfd);
	return NULL;
} /* ssl_close () */
