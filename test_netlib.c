/*
 * testutil - unit tests on functions in util.c
 *
 * by Robert (bob) Edwards, July 2021
 *
 * compile with: gcc -Wall testutil.c util.c
 *
 * GPLv2 etc.
 */

#include <assert.h>

#include "netlib.h"

char *teststr[] = {
	"10.203.34.15",
	"192.168.1.4",
    "300.100.2",
	".34",
	NULL
};

void test_ipv4AddrToDDN () {
	char buf[1024];
	char *str1 = teststr[0];
	uint32_t ip4;

	ip4 = htonl (((10 * 256 + 203) * 256 + 34) * 256 + 15);
	ipv4AddrToDDN (buf, &ip4);
	printf ("buf: %s\n", buf);
	assert (!strcmp (buf, str1));
} /* test_ipv4AddrToDDN () */

int main (int argc, char *argv[]) {
	test_get_net_num ();
	test_checksum ();
	test_ipv4AddrToDDN ();
	test_etherscanf ();
	test_mac2Str ();
	test_ipv4gai ();
	test_dns_reverse ();
	printf ("all good!\n");
	return 0;
}
